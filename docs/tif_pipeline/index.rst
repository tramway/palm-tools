PALM : from TIF files to tracks
*******************************

.. toctree::
    :maxdepth: 2
    
    pipeline/index
    data/index

Localizations and tracking
==========================

PALM-tols provides a customizable pipeline structure for extracting localizations and tracks from PALM movies. Learn more :ref:`here <tif_pipeline>`.

Data structure
==============

PALM-tols manages experimental data using two main classes : ``Experiment`` and ``Acquisition``. Learn more :ref:`here <pipeline>`.
Gratin
======

Principle : extract learnt features describing trajectories
-----------------------------------------------------------

Gratin is a tool for characterizing trajectories of random walk. 
It leverages a neural network **trained on simulated data** to infer physical properties of random walks. 
This forces it to learn a meaningful description of such trajectories, under the form of a `latent vector`. 
This vector is originally of dimension 16, but can be visualized in two dimensions using an encoder trained with a parametric UMAP.

Many metrics have already been introduced to characterize random walks, but their estimators do not necessarily 
behave well when trajectories are short or when localizations are determined with uncertainty.
Gratin circumvent this issue by learning to compute an information-rich latent-vector on trajectories 
mimicking those observed under experimental conditions in terms of localization uncertainty, length, diffusion coefficient.
Random walks used for training originiate from five different random walk models, covering a diversity of dynamics.

..include here an image

Use gratin on your trajectories
-------------------------------
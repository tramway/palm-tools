Where to start from ?
=====================

The analysis pipeline takes sets of localizations/trajectories as inputs. These sets are instances of the class ``TrackSet`` 
and are gathered into one instance of ``TrackSets`` (a bit like ``Acquisition`` and ``Experiment`` classes work). 

.. note:: 

    This section isn't about pipelines processing TIF movies. For information about these, go :ref:`here <tif_pipeline>`.

There are two main ways of proceeding : 

- either you analyze your PALM movies using PALM-tools and you create your ``TrackSets`` instance from there (see :ref:`here <from_experiment>`), 

- or you use trajectories obtained by other means and stored in CSV-like files (see :ref:`here <external>`).

.. _from_experiment:
Option 1 : from a processed experiment
--------------------------------------

You've run your favorite ``TifPipeline`` on your ``Experiment`` ? It takes just one line to get an instance of ``TrackSets`` containing your processed data !
The index table will be transferred, and the same export folder will be used.

.. code-block:: python3

    tss = exp.to_tracksets(tif_pipeline=tp)


.. _external:
Option 2 : from trajectories obtained by other means
----------------------------------------------------

If you've already spent years fine-tuning your image processing and tracking pipeline, that's fine, you can use PALM-tools analyses on your home-made trajectories !
The only requirements are that they must be stored in a CSV-like format, and contain the ``x``, ``y``, ``frame`` and ``n`` columns (the latter marking the trajectory to which detections belong).

You need to specify a folder where the analysis results will be stored, and you can as well specify an index dataframe containing 
information about the conditions in which your trajectories were obtained : 
which type of cell did you observe, which protein did you look at, how were the cells treated, ...
Here's an example of what this table might look like.

+---------+-------------+------------------+
| file    | cell_type   | observation_time |
+=========+=============+==================+
| ROI1.csv| neuron      | T+1h             |
+---------+-------------+------------------+
| ROI2.csv| astrocyte   | T+1h             |
+---------+-------------+------------------+
| ROI3.csv| neuron      | T+2h             |
+---------+-------------+------------------+

Then, create an instance of ``TrackSets`` with the following snippet :

.. code-block:: python3

    from palm_tools import TrackSets

    tss = TrackSets.from_files(
        files=["ROI1.csv","ROI2.csv","ROI3.csv"],
        root_folder="myproject/data/output",
        index_df=index_df
    )
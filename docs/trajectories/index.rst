**********************
Analyzing trajectories
**********************

.. toctree::
    :maxdepth: 2

    input
    gratin
    mmd
    mds
==========
palm-tools
==========

This is the documentation of **palm-tools**, a Python package for analyzing series of PALM acquisitions, 
from the processing of raw ``.tif`` files to the statistical comparison of trajectories observed in different biological conditions.

It comprises

.. important::

    This package is under development, if you wish to contribute, report a bug or suggest an addition, raise an issue or send an email to hverdier@pasteur.fr.

Contents
========

.. toctree::
   :maxdepth: 3

   Handling TIF files <tif_pipeline/index>
   Analyzing trajectories <trajectories/index>
   Authors <authors>
   License <license>
   Module Reference <api/modules>

.. doctest::
    assert True

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
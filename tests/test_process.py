import palm_tools
import pandas as pd
from glob import glob
import logging
from palm_tools.utils.read_file import check_file_and_read, df_is_valid

__author__ = "Hippolyte Verdier"
__copyright__ = "Hippolyte Verdier"
__license__ = "MIT"


# def test_main(capsys):
#    """CLI Tests"""
#    # capsys is a pytest fixture that allows asserts agains stdout/stderr
#    # https://docs.pytest.org/en/stable/capture.html
#    main(["--data data"])
#    captured = capsys.readouterr()
#    assert "The 7-th Fibonacci number is 13" in captured.out


def df_is_valid(df: pd.DataFrame, is3D: bool = False):
    columns = ["x", "y", "t", "n", "frame"]
    if is3D:
        columns.append("z")
    for col in columns:
        if col not in df.columns:
            return False
    return True


def test_read_trackfiles(capsys):
    for file_path in glob("tests/data/tracks/2D/*"):
        df, DT = check_file_and_read(file_path=file_path, is3D=False)
        logging.info(file_path)
        logging.info("df = %s" % df)
        logging.info("DT = %.3f" % float(DT))
        assert df_is_valid(df, is3D=False)
    for file_path in glob("tests/data/tracks/3D/*"):
        df, DT = check_file_and_read(file_path=file_path, is3D=True)
        logging.info(file_path)
        logging.info("df = %s" % df)
        logging.info("DT = %.3f" % float(DT))
        assert df_is_valid(df, is3D=True)

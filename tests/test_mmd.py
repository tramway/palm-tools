from palm_tools.analysis.mmd_analysis import MMDInterGroupAnalysis, MMDInterUnitAnalysis
from palm_tools.analysis.mds_analysis import MDSAnalysis
from palm_tools.data_structure.trackset import TrackSets
import logging
import numpy as np
from palm_tools.post_processing.ts_post_processing import PostProcessingStepSeries
from palm_tools.post_processing.gratin import GratinAllLengths, GratinParameters
from palm_tools.post_processing import StepsAndDiffusion
from itertools import combinations

def get_valid_factors_combinations(hierarchy):
    factors_combinations = []
    for max_level in sorted(hierarchy.values()):
        mandatory_factors = [f for f, level in hierarchy.items() if level < max_level]
        optional_factors = [f for f, level in hierarchy.items() if level == max_level]
        for i in range(len(optional_factors)):
            for opt_f in combinations(optional_factors,i+1):
                factors_combinations.append(mandatory_factors + list(opt_f))
    factors_combinations = factors_combinations + [["file"]]
    return factors_combinations

def test_mmd_runs():
    # Write tracksets from folder
    # Tracksets from folder
    
    model_path = "tests/data/models/default"
    logger = logging.getLogger("Gratin")
    gt = GratinAllLengths(params=GratinParameters(path=model_path),logger=logger)
    sd = StepsAndDiffusion()
    pps = PostProcessingStepSeries(processing_steps=[gt, sd],logger=logger)
    
    tss = TrackSets.from_folder("tests/data/experience", root_folder="tests/data/exports",file_pattern="*.locs")
    tss.index_df["Activation"] = tss.index_df.file.str.split("/",expand=True)[3]
    tss.index_df["Experience"] = tss.index_df.file.str.split("/",expand=True)[4]
    tss.index_df["CellZone"  ] = np.arange(tss.index_df.shape[0])
    
    pps.process(tss, force=True)
    
    hierarchy = {"Activation":0,"Experience":1,"CellZone":2}
    
    mmd = MMDInterGroupAnalysis(
        run_name="test",
        track_sets=tss,
        group_by_keys=["Activation","CellZone"],
        hierarchy=hierarchy,
        unit_key=["file"],
        #null_mode="mix",
        n_processes=1,
        logger=logger,
        )
    logging.info("%d real comparisons" % len(mmd.equivalent_comparisons))
    logging.info("%d Groups" % mmd.n_groups)
    mmd.process(force_recompute=True)
    
    logging.info("Starting inter unit comparisons")
    
    valid_factors = get_valid_factors_combinations(hierarchy)
    
    logging.info("Valid factors : %s" % (valid_factors))
    
    for factors in valid_factors:
        
        factors_name = "_".join(factors)
        
        logging.info("Factors : %s" % factors_name)
        
        mmd = MMDInterUnitAnalysis(track_sets=tss,
                                group_by_keys=factors,
                                # it is possible to delimit units by a column of the trajectories files (if they have additional information)
                                # or to consider a cartesian product of two columns.
                                # example of valid values : ["file","time_bin"], ["file","organelle"], ["organelle"] (spanned across files)
                                use_high_dimension=False,
                                unbiased=False,
                                n_processes=4,
                                n_bootstraps_D_sigma=10,
                                n_max_trajs_per_unit=500,  # For speed, we never consider more than 500 trajectories per unit
                                n_min_trajs_per_unit=50,  # For accuracy, we discard units with less than 150 trajectories
                                run_name=factors_name)
        
        if len(mmd.groups) > 2:
            mmd.process(force_recompute=True)
            logging.info("Starting MDS")
            mds = MDSAnalysis(tss, run_name=factors_name,mmd_name=factors_name, logger=logger)
            mds.process(force_recompute=True)
        else:
            logging.info("Too few groups, skipping")
    
    assert True
    
    
    
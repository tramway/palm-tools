.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/palm-tools.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/palm-tools
    .. image:: https://readthedocs.org/projects/palm-tools/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://palm-tools.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/palm-tools/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/palm-tools
    .. image:: https://img.shields.io/pypi/v/palm-tools.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/palm-tools/
    .. image:: https://img.shields.io/conda/vn/conda-forge/palm-tools.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/palm-tools
    .. image:: https://pepy.tech/badge/palm-tools/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/palm-tools
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/palm-tools

==========
palm-tools
==========


    A Python tool to process your PALM ``.tif`` files.
    Includes visualisations of results using the `napari <https://napari.org>`_ viewer

To run localization and tracking on all .tif files in a directory (and its subdirectories), use the following command

.. code-block:: bash

    usage: python -m palm_tools.process [-h] [-n NUM_PROCESSES] (--default | --run RUN_NAME) [--export EXPORT] [--data DATA] [-v] [-vv]

    This command runs localization and tracking on .tif files of PALM acquisition located in --data directory. 
    It stores results in the --export directory. It can be run with default settings, or custom ones. 
    In the latter case, the user will be prompted to indicates the desired settings. 
    If some .tif files were already processed with the given settings, they will not be re-processed

    optional arguments:
    -h, --help           show this help message and exit
    -n NUM_PROCESSES     Number of processes to run concurrently
    --default            Use default settings if present
    --run RUN_NAME       Name of this run, if settings are not default
    --export EXPORT      Folder to export processed data
    --data DATA          Folder where the .tif files are
    -v, --verbose        set loglevel to INFO
    -vv, --very-verbose  set loglevel to DEBUG

To visualize or post-process trajectories or localizations, access them through the ``Experiment`` and ``Acquisition`` classes

.. code-block:: python

    # An experiment is a set of acquisitions processed together
    exp = Experiment(data_folder=DATA_FOLDER,export_folder=EXPORT_FOLDER)
    # Lists all available runs of processing previously run for this experiment
    runs = exp.available_processing_runs()
    run = runs[0]
    # Lists all *.tif files in the experiment which were processed with parameters of this run
    acq_files = exp.all_files(only_processed_with_run=run)
    # Instiantiate an Acquisition
    acq = Acquisition(acq_files[0],experiment=exp,tif_pipeline=run)
    # Visualize in Napari localizations and trajectories found using the parameters of this run
    acq.view(min_traj_length=5)
    # Scan runs and plots the effect of a processing parameter on some statistics of the processed data 
    # (number of localizations, number of trajectories longer than 7 or 15 points)
    exp.parameter_influence_on_stats(param_name="sliding_window_filter",stats=["n_locs","n_trajs_7","n_trajs_15"],mode="hist")


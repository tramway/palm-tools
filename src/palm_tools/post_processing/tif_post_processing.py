from __future__ import annotations
import logging
import os
from typing import TYPE_CHECKING, Iterable, List

from abc import ABC, abstractmethod
from tqdm import tqdm

if TYPE_CHECKING:
    from palmari import Experiment, TifPipeline, Acquisition
    from .post_processing_params import PostProcessingParameters


class TifPostProcessingStep(ABC):
    def __init__(self, params: PostProcessingParameters):
        self.parameters = params

    @property
    @abstractmethod
    def step_name(self):
        return "Base class, step without a name"

    @property
    @abstractmethod
    def locs_column_names(self) -> List[str]:
        return []

    def was_already_processed(self, acq: Acquisition):
        # For now, only check that the column exists
        # TODO: check that the processing was done using the same parameters !
        for col in self.locs_column_names:
            if col not in acq.locs.columns:
                return False
        return True

    def process(self, acq: Acquisition, force=False):
        """
        Runs this step on the trackset
        1) Check if the computation has already been done
        2) If necessary, run the computation
        3) Save its locs table

        Args:
            trackset (Trackset): the trackset on which to run the processing step
        """
        if self.was_already_processed(acq) and not force:
            logging.info(
                "Acquisition %s was already processed by %s"
                % (acq.tif_file, self.step_name)
            )
        else:
            logging.info("Running %s on %s" % (self.step_name, acq.tif_file))
            self._process(acq)
            acq._save_locs()

    @abstractmethod
    def _process(self, acq: Acquisition):
        """
        Where the real processing happens
        Should be overriden by sub-classes

        Args:
            acq (Acquisition): acquisition on which to perform processing
        """
        pass


class TifPostProcessingStepSeries:
    def __init__(
        self,
        tif_processing: TifPipeline,
        processing_steps: List[TifPostProcessingStep],
    ):
        self.tif_processing = tif_processing
        self.processingSteps = processing_steps

    def process(self, exp: Experiment, force=False):
        from palmari import Acquisition

        pgbar = tqdm(exp, desc="TifPostProcessing...")
        for f in pgbar:
            acq = Acquisition(
                f,
                experiment=exp,
                tif_pipeline=self.tif_processing,
            )
            for step in self.processingSteps:
                pgbar.set_postfix_str(acq.tif_file + " : " + step.step_name)
                step.process(acq, force=force)
            del acq

from __future__ import annotations
import logging

from palm_tools.data_structure.trackset import TrackSet
from .ts_post_processing import PostProcessingStep
from .post_processing_params import PostProcessingParameters
from typing import List
import numpy as np
import pandas as pd
from tqdm import tqdm
import os


class GratinParameters(PostProcessingParameters):
    def __init__(self, 
                 path: str, 
                 use_high_dim: bool = False):
        super().__init__({
            "path": path, 
            "use_high_dim": use_high_dim})

    @property
    def defaults(self):
        return {"path": None, "use_high_dim": False}


class Gratin(PostProcessingStep):
    def __init__(self, params: GratinParameters, logger: logging.Logger = None):
        super().__init__(params, logger=logger)

    @classmethod
    def train_model(
        cls,
        export_path: str,
        time_delta: float,
        num_workers: int = 0,
        max_length: int = 35,
        dim: int = 2,
        log_diffusivity_range: tuple = (-2.1, 1.1),
        localization_error_range: tuple = (0.015, 0.05),
    ):
        from gratin.standard import train_model

        train_model(
            export_path=export_path,
            time_delta=time_delta,
            num_workers=num_workers,
            dim=dim,
            log_diffusion_range=log_diffusivity_range,
            noise_range=localization_error_range,
            length_range=(7, max_length),
        )

    @property
    def step_name(self):
        return "Gratin latent vectors and output"

    @property
    def h_cols(self):
        """
        Columns of the latent vector
        """
        n_dims = self.model.hparams["latent_dim"]
        return ["h_%d" % (i + 1) for i in range(n_dims)]

    @property
    def locs_column_names(self) -> List[str]:
        base = ["U_1", "U_2"]
        if ("predict_alpha" not in self.model.hparams) or (self.model.hparams["predict_alpha"]):
            base.append("alpha")
        if ("predict_model" not in self.model.hparams) or (self.model.hparams["predict_model"]):
            base.append("best_model")
        if self.params_dict["use_high_dim"]:
            base = base + self.h_cols
        return base

    @property
    def model(self):
        from gratin.models.main_net import MainNet
        if not hasattr(self, "_model"):
            self._model = MainNet.load_from_checkpoint(
                os.path.join(self.parameters.params_dict["path"], "model.ckpt")
            )
            self._model.eval()

        return self._model

    @property
    def encoder(self):
        from tensorflow.keras.models import load_model

        if not hasattr(self, "_encoder"):
            self._encoder = load_model(
                os.path.join(self.parameters.params_dict["path"], "umap")
            )
        return self._encoder

    @property
    def model_cols(self):
        if not hasattr(self, "_model_cols"):
            if "best_model" in self.locs_column_names:
                self._model_cols = ["p_%s" % m for m in self.model.hparams["RW_types"]]
            else:
                self._model_cols = []
        return self._model_cols

    def _process(self, ts: TrackSet):
        from gratin.standard import get_predictions

        df = get_predictions(
            model=self.model, encoder=self.encoder, trajectories=ts.locs
        )
        
        cols_to_add = self.locs_column_names + self.model_cols
        if self.params_dict["use_high_dim"]:
            print("Adding h cols")
            cols_to_add = cols_to_add + self.h_cols
        else:
            print("Dropping h cols")
        ts.add_traj_cols_to_locs(df[cols_to_add])


class GratinAllLengths(Gratin):
    def __init__(self, params: GratinParameters,logger: logging.Logger = None):
        super().__init__(params,logger=logger)

    @property
    def step_name(self):
        return "Gratin latent vectors and output (all lengths)"

    @property
    def locs_column_names(self) -> List[str]:
        return []

    @property
    def trajs_column_names(self) -> List[str]:
        return super().locs_column_names

    def _process(self, ts: TrackSet):
        from gratin.standard import get_predictions

        locs = ts.locs[["frame"] + ts.coord_cols + ["n", "t"]].sort_values(["n", "t"])
        traj_lengths = locs.n.value_counts()
        L_max = min(np.quantile(traj_lengths.values, 0.95), 50)

        dfs = []

        f = 1.3

        max_n_lengths_per_traj = int(np.floor(np.log(L_max) / np.log(f)))

        # logging.info("%d length steps" % max_n_lengths_per_traj)

        for k in range(max_n_lengths_per_traj):

            f_k = np.power(f, k)

            trajs_to_consider = traj_lengths.loc[traj_lengths / f_k >= 7].index.tolist()
            if len(trajs_to_consider) == 0:
                logging.debug("No trajs for factor %.2f" % f_k)
                break

            def shorten_traj(t):
                L = int(t.shape[0] / f_k)
                return t.iloc[:L]

            locs_L = (
                locs.loc[locs.n.isin(trajs_to_consider)]
                .groupby("n")
                .apply(shorten_traj)
            )
            locs_L.reset_index(drop=True, inplace=True)

            df_L = get_predictions(
                model=self.model, encoder=self.encoder, trajectories=locs_L
            )
            shortened_traj_length = locs_L.n.value_counts()
            df_L["L"] = shortened_traj_length
            df_L.reset_index(inplace=True)
            df_L.rename(columns={"index": "n"}, inplace=True)

            dfs.append(df_L)

        df = pd.concat(dfs, axis=0)

        cols_to_add = ["n", "L"] + self.trajs_column_names + self.model_cols
        if self.params_dict["use_high_dim"]:
            # Only add high_dim columns if necessary
            cols_to_add = cols_to_add + self.h_cols
        ts.add_traj_cols_to_trajs_df(df[cols_to_add])

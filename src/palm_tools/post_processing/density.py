from __future__ import annotations

from palm_tools.data_structure.trackset import TrackSet
from .ts_post_processing import PostProcessingStep
from .post_processing_params import EmptyPostProcessingParameters
from typing import TYPE_CHECKING, List
import numpy as np
from sklearn.neighbors import KernelDensity
import pandas as pd


class Density(PostProcessingStep):
    def __init__(self):
        params = EmptyPostProcessingParameters()
        super().__init__(params)

    @property
    def locs_column_names(self) -> List[str]:
        return ["abs_density", "norm_density"]

    @property
    def step_name(self):
        return "Compute density"

    def _process(self, ts: TrackSet):

        kde = KernelDensity(bandwidth=0.025, kernel="gaussian", rtol=0.02)
        N_true = ts.locs.shape[0]
        N_fit = min(N_true, 100000)
        kde.fit(ts.locs[["x", "y"]].sample(N_fit).values)
        ds = pd.DataFrame(index=ts.locs.index)
        ds["abs_density"] = (
            np.exp(kde.score_samples(ts.locs[["x", "y"]].values)) * N_true / N_fit
        )
        L_x = ts.locs.x.max() - ts.locs.x.min()
        L_y = ts.locs.y.max() - ts.locs.y.min()

        ds["norm_density"] = ds["abs_density"] / (N_true / (L_x * L_y))
        ts.add_columns_to_loc(ds[self.locs_column_names])

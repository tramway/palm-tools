from __future__ import annotations
import logging
import os

from palmari.processing.utils import get_values_as_in_dict
from abc import ABC, abstractmethod
import json


class PostProcessingParameters(ABC):
    def __init__(self, params: dict = {}):
        for key in params:
            assert (
                key in self.defaults
            ), "Invalid parameter %s, possible options are %s" % (
                key,
                ", ".join(self.defaults.keys()),
            )

        self.params_dict = dict(self.defaults)
        self.params_dict.update(params)

        for key, value in self.params_dict.items():
            assert value is not None, (
                "%s was not precised but should have been (no default value available)"
                % key
            )

    @property
    @abstractmethod
    def defaults(self):
        return {}

    @classmethod
    def from_json(cls, json_file: str, use_defaults=False) -> PostProcessingParameters:
        try:
            json_dict = json.load(open(json_file, "r"))
            logging.info(json_dict)
            return cls(params_dict=json_dict)
        except FileNotFoundError:
            logging.debug("File %s does not exist" % json_file)
            if use_defaults:
                return cls()
            else:
                user_inputed_dict = cls.get_user_inputed_params_dicts()
                return cls(params_dict=user_inputed_dict)

    def is_equal_to_json(self, json_file):
        if os.path.exists(json_file):
            pre_existing = json.load(open(json_file, "r"))
            for key, value in pre_existing.items():
                try:
                    assert value == self.params_dict[key]
                except AssertionError:
                    print("%s is %s in stored params" % (key, value))
                    print("But %s in to_save" % self.params_dict[key])
                    raise

    def save_at(self, json_file: str):
        assert self.is_equal_to_json(json_file)
        json.dump(self.params_dict, open(json_file, "w"))

    @classmethod
    def get_user_inputed_params_dicts(cls) -> dict:
        params = get_values_as_in_dict(
            cls.defaults,
            summary_string="Please enter %s parameters" % cls.name,
        )

        return params

    @property
    def hash_dict(self):
        return self.params_dict.hash


class EmptyPostProcessingParameters(PostProcessingParameters):
    @property
    def defaults(self):
        return {}

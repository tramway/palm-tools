from __future__ import annotations
import secrets
from .ts_post_processing import PostProcessingStep
from .post_processing_params import EmptyPostProcessingParameters

# from palm_tools.tif_tools.cell_mask import tag_localizations_per_cell, get_cell_mask
from typing import TYPE_CHECKING, List
import pandas as pd
import numpy as np

if TYPE_CHECKING:
    from palmari import Acquisition


class TagCells(PostProcessingStep):
    def __init__(self):
        params = EmptyPostProcessingParameters()
        super().__init__(params)

    @property
    def locs_column_names(self) -> List[str]:
        return ["cell_ID"]

    @property
    def step_name(self):
        return "Tag cells"

    def _process(self, acquisition: Acquisition):

        locs = acquisition.locs
        labels = get_cell_mask(acquisition.image)
        r = tag_localizations_per_cell(
            locs, labels, scale=acquisition.experiment.pixel_size
        )
        result = pd.DataFrame(index=r.index)
        result[self.locs_column_names[0]] = r.values
        IDs_2_hash = {}
        for v in r.unique().tolist():
            IDs_2_hash[v] = secrets.token_hex(12)
        IDs_2_hash[0] = None
        result[self.locs_column_names[0]].replace(IDs_2_hash, inplace=True)

        acquisition.add_columns_to_loc(result)

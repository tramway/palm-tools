from __future__ import annotations

from palm_tools.data_structure.trackset import TrackSet
from .ts_post_processing import PostProcessingStep
from .post_processing_params import EmptyPostProcessingParameters
from typing import TYPE_CHECKING, List
import numpy as np
import pandas as pd
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist
import warnings
import logging

MAX_LENGTH = 500


class ConvexHullAsymmetry(PostProcessingStep):
    def __init__(self, logger: logging.Logger = None):
        super().__init__(EmptyPostProcessingParameters(), logger = logger)

    @property
    def locs_column_names(self) -> List[str]:
        return [
            "diameter",
            "qhull_volume",
            "qhull_surface",
            "asymmetry",
            "fractal_dimension",
        ]

    @property
    def step_name(self):
        return "Convex hull and assymmetry"

    def _process(self, ts: TrackSet):
        traj_length = ts.locs.n.value_counts()
        good_trajs = traj_length.loc[traj_length >= 3].index.tolist()
        tracks = ts.locs.loc[
            ts.locs.n.isin(good_trajs), ts.coord_cols + ["n", "t"]
        ].sort_values(["n", "t"])

        def get_qhull(traj_):
            traj = traj_.iloc[: min(MAX_LENGTH, traj_.shape[0])]
            qhull = ConvexHull(traj[ts.coord_cols].values)
            t = {}
            t["qhull_surface"] = qhull.area
            t["qhull_volume"] = qhull.volume
            D = pdist(traj[ts.coord_cols])
            d = np.max(D)
            t["diameter"] = d
            T = np.cov(traj[ts.coord_cols].T)
            lambda_1, lambda_2 = list(np.linalg.eigvals(T))[:2]
            t["asymmetry"] = -np.log(
                1 - (((lambda_1 - lambda_2) ** 2) / (2 * (lambda_1 + lambda_2)))
            )
            L = np.sum(
                np.sqrt(np.sum((traj.values[1:] - traj.values[:-1]) ** 2, axis=1))
            )
            N = traj.shape[0]
            with warnings.catch_warnings():
                warnings.simplefilter("error")
                t["fractal_dimension"] = np.log(N) / np.log(N * d / L)
            return pd.DataFrame.from_records([t]).reset_index()

        tracks_properties = tracks.groupby("n").apply(get_qhull)
        tracks_properties.reset_index(level=1, drop=True, inplace=True)
        ts.add_traj_cols_to_locs(tracks_properties)


class StepsAndDiffusion(PostProcessingStep):
    def __init__(self,logger: logging.Logger = None):
        params = EmptyPostProcessingParameters()
        super().__init__(params,logger=logger)

    @property
    def locs_column_names(self) -> List[str]:
        return ["log_D", "D", "est_sigma", "n_points", "duration"]

    @property
    def step_name(self):
        return "Step length and diffusion"

    def _process(self, ts: TrackSet):

        traj_length = ts.locs.n.value_counts()
        good_trajs = traj_length.loc[traj_length >= 2].index.tolist()
        tracks = ts.locs.loc[
            ts.locs.n.isin(good_trajs), ts.coord_cols + ["n", "t"]
        ].sort_values(["n", "t"])
        is_last = tracks["n"] != tracks.shift(-1).fillna(-1)["n"]
        is_first = tracks["n"] != tracks.shift(1).fillna(-1)["n"]
        dr_2 = ((tracks.shift(-1)[ts.coord_cols] - tracks[ts.coord_cols]) ** 2).mean(axis=1)
        dr_next = tracks.shift(-1)[ts.coord_cols] - tracks[ts.coord_cols]
        dr_prev = tracks[ts.coord_cols] - tracks[ts.coord_cols].shift(1)
        dt_next = (tracks.shift(-1)["t"] - tracks["t"]).abs()
        tracks["dt"] = dt_next
        dt_prev = (tracks["t"] - tracks.shift(1)["t"]).abs()
        dr_corr = (dr_next * dr_prev).mean(axis=1)
        tracks["dr_corr_t"] = dr_corr / np.sqrt(dt_next * dt_prev)
        tracks["dr_2_t"] = dr_2 / (2 * dt_next)

        # These will be used to compute the diffusivity estimate
        # That's why they're divided by delta_t
        mean_dr_2 = tracks.loc[~is_last].groupby("n").aggregate({"dr_2_t": "mean"})
        mean_dr_corr = (
            tracks.loc[~is_last & ~is_first]
            .groupby("n")
            .aggregate({"dr_corr_t": "mean"})
        )
        R = 1.0 / 6.0  # (maximally open shutter, see Vestergaard PRE 2014)

        # This is to ponderate the dr by the DT / dt
        # Exposure
        DT = (
            tracks.loc[~is_last]
            .groupby("n")[["dt"]]
            .mean()
            .rename(columns={"dt": "mean_dt"})
        )
        tracks = pd.merge(tracks, DT, left_on="n", right_index=True, how="left")
        # dt might differ from DT because some points might be missing
        # not sure how well this weighting works though...
        tracks["dr_2_sigma"] = dr_2 * (tracks["mean_dt"] / dt_next)
        tracks["dr_corr_sigma"] = dr_corr * (
            tracks["mean_dt"] / np.sqrt(dt_next * dt_prev)
        )

        sigma_2 = np.clip(
            R
            * tracks.loc[~is_last].groupby("n").aggregate({"dr_2_sigma": "mean"}).values
            + (2 * R - 1)
            * tracks.loc[~is_last & ~is_first]
            .groupby("n")
            .aggregate({"dr_corr_sigma": "mean"})
            .values,
            a_min=0,
            a_max=np.inf,
        )

        diffusivity = mean_dr_2.values + mean_dr_corr.values
        diffusivity[diffusivity < 0.] = mean_dr_2.values[diffusivity < 0.]

        trajs_info = (
            tracks.groupby("n")
            .aggregate({"x": "count", "t": "max"})
            .rename(columns={"x": "n_points", "t": "duration"})
        )

        trajs_info["D"] = diffusivity
        trajs_info["log_D"] = np.log10(diffusivity)
        trajs_info["est_sigma"] = np.sqrt(sigma_2)

        computed_cols = ["log_D", "D", "est_sigma", "n_points", "duration"]
        for c in computed_cols:
            if c in tracks:
                del tracks[c]

        tracks = tracks.merge(
            trajs_info[computed_cols],
            left_on="n",
            right_index=True,
            how="left",
        )

        ts.add_columns_to_loc(tracks[self.locs_column_names])

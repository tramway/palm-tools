from __future__ import annotations
import logging
import os
from typing import TYPE_CHECKING, Iterable, List
from logging import Logger
from palm_tools.data_structure.trackset import TrackSets

from ..data_structure.trackset import TrackSet
from abc import ABC, abstractmethod
import json
from tqdm import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm

if TYPE_CHECKING:
    from palmari import Experiment
    from .post_processing_params import PostProcessingParameters


class PostProcessingStep(ABC):
    def __init__(self, params: PostProcessingParameters, logger: Logger = None):
        if logger is None:
            logger = logging.getLogger()
        self.parameters = params
        self.logger = logger

    @property
    @abstractmethod
    def step_name(self):
        return "Base class, step without a name"

    @property
    @abstractmethod
    def locs_column_names(self) -> List[str]:
        return []

    @property
    def params_dict(self):
        return self.parameters.params_dict

    @property
    def trajs_column_names(self) -> List[str]:
        return []

    def was_already_processed(self, ts: TrackSet):
        # For now, only check that the column exists
        # TODO: check that the processing was done using the same parameters !
        for col in self.locs_column_names:
            if col not in ts.locs.columns:
                return False
        for col in self.trajs_column_names:
            if col not in ts.trajs_df.columns:
                return False
        return True

    def process(self, ts: TrackSet, force=False):
        """
        Runs this step on the trackset
        1) Check if the computation has already been done
        2) If necessary, run the computation
        3) Save its locs table

        Args:
            trackset (Trackset): the trackset on which to run the processing step
        """
        if self.was_already_processed(ts) and not force:
            self.logger.debug(
                "TrackSet %s was already processed by %s"
                % (ts.origin_file, self.step_name)
            )
        else:
            self.logger.debug("Running %s on %s" % (self.step_name, ts.origin_file))
            self._process(ts)
            ts._save_locs()

    @abstractmethod
    def _process(self, ts: TrackSet):
        """
        Where the real processing happens
        Should be overriden by sub-classes

        Args:
            ts (TrackSet): trackset on which to perform processing
        """
        pass


class PostProcessingStepSeries:
    def __init__(
        self,
        processing_steps: List[PostProcessingStep],
        logger: Logger = None,
    ):
        self.processingSteps = processing_steps
        if logger is None:
            logger = logging.getLogger()
        self.logger = logger

    def process(self, sets_of_trajs, force=False, skip_first: int = 0):
        assert skip_first <= len(sets_of_trajs)
        pgbar = tqdm(enumerate(sets_of_trajs),
                desc="Processing in PPSS : ",
                total=len(sets_of_trajs),
                disable=len(sets_of_trajs) < 10 or any([isinstance(handler, logging.FileHandler) for handler in self.logger.handlers]))
        for i, track_set in pgbar:
            
            self.logger.info("Processing file %d / %d [%s]" % (i+1,len(sets_of_trajs),track_set.origin_file))
            
            if i < skip_first:
                continue
            for step in self.processingSteps:
                pgbar.set_postfix_str(track_set.origin_file + " : " + step.step_name)
                step.process(track_set, force=force)

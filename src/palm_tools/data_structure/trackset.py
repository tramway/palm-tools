from __future__ import annotations
from typing import TYPE_CHECKING, Callable, Dict, Iterable, List
import os
import pandas as pd
from palm_tools.utils.read_file import check_file_and_read
import logging
import glob


import numpy as np

if TYPE_CHECKING:
    from palmari import Acquisition
    from palmari import Experiment
    from palmari import TifPipeline


class TrackSets:
    def __init__(
        self,
        tracksets: Iterable[TrackSet],
        root_folder: str,
        index_df: pd.DataFrame = None,
    ):
        self.sets = tracksets
        self.root_folder = root_folder

        origin_files = [ts.origin_file for ts in iter(tracksets)]
        if index_df is None:
            index_df = pd.DataFrame(index=np.arange(len(origin_files)))
            index_df["file"] = origin_files
        self.index_df = index_df
        assert len(set([frozenset(ts.coord_cols) for ts in tracksets])) == 1, "Cannot group files with different numbers of dimensions (2D and 3D trajectories)"

    def __iter__(self):
        return iter(self.sets)

    def __getitem__(self, item):
        return self.sets[item]

    def __len__(self):
        return self.index_df.shape[0]

    def __str__(self):
        return "%d sets of trajs, root_folder is %s" % (len(self), self.root_folder)

    @classmethod
    def from_files(
        cls, trajs_files: List[str], root_folder: str, index_df: pd.DataFrame = None, is3D: bool = False,
    ):
        return cls(
            tracksets=[TrackSet(f,is3D=is3D) for f in trajs_files],
            root_folder=root_folder,
            index_df=index_df,
        )
        
    @classmethod
    def from_folder(
        cls, data_folder: str, root_folder: str, file_pattern : str = "*.locs", index_df: pd.DataFrame = None
    ):
        search_regex = os.path.join(data_folder,"**",file_pattern)
        files = glob.glob(search_regex,recursive=True)
        return cls.from_files(
            trajs_files=files,
            root_folder=root_folder,
            index_df=index_df,
        )

    @classmethod
    def from_experiment(cls, exp: Experiment, tp: TifPipeline):
        from palmari import Acquisition

        index_df = exp.index_df
        index_df["tif_file"] = index_df[
            "file"
        ].copy()  # We don't want 'file' to refer to the TIF file
        replace_dict = {}
        for f in exp:
            replace_dict[f] = Acquisition(f, exp, tp).locs_path
        index_df["file"] = index_df["file"].replace(replace_dict)

        return TrackSets.from_files(
            [Acquisition(f, exp, tp).locs_path for f in exp],
            root_folder=exp.export_folder,
            index_df=index_df,
        )
        
    @property
    def coord_cols(self):
        if not hasattr(self, "_coord_cols"):
            self._coord_cols = self.sets[0].coord_cols
        return self._coord_cols


class TrackSet:
    def __init__(
        self,
        locs_path: str,
        origin_file: str = None,
        # columns of the locs file which contain trajectory-level information and should be kept in self.trajs_df (format : agg_dict)
        info_columns: Dict = {},
        is3D: bool = False,
    ):
        self.locs_path = locs_path
        self.is3D = is3D
        if origin_file is None:
            origin_file = locs_path
        self.origin_file = origin_file
        self.info_columns = info_columns
        self.locs

    @classmethod
    def from_acquisition(
        cls,
        acquisition: Acquisition,
        min_length: int = 7,
        force_recompute: bool = False,
    ):
        assert (
            acquisition.is_processed
        ), "Can only take trajectories from processed acquisitions"
        assert min_length >= 7, "should only keep trajectories longer than 7 points"

        export_path = acquisition.tif_pipeline.data_path_with_extension(
            acquisition, ".trackset"
        )
        should_reload = force_recompute or (not os.path.exists(export_path))

        if not should_reload and os.path.exists(export_path):
            locs_existing = pd.read_csv(export_path, index_col=0)
            missing_columns = set(list(acquisition.locs.columns)).difference(
                list(locs_existing.columns)
            )

            if len(missing_columns) > 0:
                logging.info(
                    "Missing columns : overwriting Trajs file for %s"
                    % acquisition.tif_file
                )
                should_reload = True

        if should_reload:
            print("reloading")
            df = acquisition.locs
            _, good_trajs = acquisition.trajectories_list(
                min_length=min_length, return_indices=True
            )
            df.loc[df.n.isin(good_trajs)].to_csv(export_path)
        else:
            # PROBLEM: min_length parameter is not considered
            logging.info(
                "Trajs file was already computed for %s, we just load it"
                % acquisition.tif_file
            )
        ts = TrackSet(locs_path=export_path, origin_file=acquisition.tif_file)
        return ts

    def __str__(self):
        return "Set of %d trajs coming from file %s" % (
            self.locs.n.nunique(),
            self.origin_file,
        )

    @property
    def trajs_df_path(self):
        return self.locs_path + ".trajs"
    
    @property
    def coord_cols(self):
        if not hasattr(self, "_coord_cols"):
            self.locs
            logging.debug("Should not query coord cols before loading self._locs")
        return self._coord_cols

    @property
    def locs(self):
        if not hasattr(self, "_locs"):
            try:
                self._locs, err = check_file_and_read(self.locs_path,is3D=self.is3D)
                assert self._locs is not None, "Could not read %s : %s" % (self.locs_path, err)
            except Exception as e:
                logging.error("Cannot load locs from %s" % self.locs_path)
                logging.error("Originating from %s" % self.origin_file)
                raise e
            
            self._coord_cols = ["x","y"]
            if "z" in self._locs:
                self._coord_cols = self._coord_cols + ["z"]
            
            null_rows = (
                self._locs[self.coord_cols + ["n", "t", "frame"]].isnull().sum(axis=1) > 0
            )
            if null_rows.sum() > 0:
                    logging.warn(
                    "%s :\tThere were null rows (%d / %d) in added dataframe (when loading locs)"
                    % (str(self), null_rows.sum(), null_rows.shape[0])
                )
            self._locs = self._locs.loc[~null_rows]
            traj_length = self._locs.n.value_counts()
            good_trajs = traj_length.loc[traj_length >= 7].index.tolist()
            self._locs = self._locs[
                [c for c in self._locs.columns if "Unnamed" not in c]
            ]
            self._locs = self._locs.loc[self._locs.n.isin(good_trajs)]
            
            self._make_dfs_categorical()
            self._locs[self.coord_cols + ["t"]] = self._locs[self.coord_cols + ["t"]].astype(float)

            self._locs["n"] = self._locs["n"].astype(int)
            self._locs["frame"] = self._locs["frame"].astype(int)

        return self._locs

    @property
    def trajs_df(self) -> pd.DataFrame:
        """
        Data Frame with information stored at the (trajectory x length) level
        The index is thus (n,L) with L ranging from 7 to the true trajectory length
        Values in columns are computed for the sub-trajectory composed of the first L points of the trajectory
        Returns:
            pd.DataFrame: _description_
        """
        if not hasattr(self, "_trajs_df"):
            try:
                self._trajs_df = pd.read_csv(self.trajs_df_path)
                self._trajs_df = self._trajs_df[
                    [
                        c
                        for c in self._trajs_df.columns
                        if "Unnamed" not in c and "_x" not in c and "_y" not in c
                    ]
                ]
                self._trajs_df = (
                    self._trajs_df.groupby(["n", "L"]).first().reset_index()
                )
                logging.debug("Successfully read trajs_df file %s" % self.trajs_df_path)
            except FileNotFoundError:
                logging.debug("Could not find trajs file : %s" % self.trajs_df_path)
                traj_lengths = self.locs.n.value_counts()
                L_max = traj_lengths.max()
                indices = []
                for L in range(7, L_max + 1):
                    trajs_to_consider = traj_lengths.loc[
                        traj_lengths >= L
                    ].index.tolist()
                    indices += [(n, L) for n in trajs_to_consider]
                _trajs_df = pd.DataFrame(index=np.arange(len(indices)))
                _trajs_df["n"] = [i[0] for i in indices]
                _trajs_df["L"] = [i[1] for i in indices]
                if len(self.info_columns) > 0:
                    infos = self.locs.loc.groupby("n").agg(self.info_columns)
                    self._trajs_df = _trajs_df.merge(
                        infos, left_on="n", right_index=True, how="left"
                    )
                else:
                    self._trajs_df = _trajs_df
                logging.debug("Created trajs_df with %d rows" % self._trajs_df.shape[0])

            self._make_dfs_categorical()

        assert self._trajs_df[["n", "L"]].duplicated().sum() == 0

        return self._trajs_df

    def _save_trajs_df(self):
        self.trajs_df.to_csv(self.trajs_df_path, index=False)

    def _save_locs(self):
        self._locs.to_csv(self.locs_path)

    def add_columns_to_loc(self, columns: pd.DataFrame):  # , save: bool = False):

        for c in columns.columns:
            if c in self._locs.columns:
                del self._locs[c]
        self._locs = self._locs.merge(
            columns, left_index=True, right_index=True, how="left"
        )
        # if save:
        #    self._save_locs()

    def add_traj_cols_to_locs(self, traj_columns: pd.DataFrame):
        """
        traj_columns is a dataframe whose index corresponds to the 'n' column (traj ID)
        We merge it with the locs dataframe
        """
        to_delete_cols = [c for c in traj_columns.columns if c in self._locs.columns]
        for col in to_delete_cols:
            del self._locs[col]
        self._locs = self._locs.merge(
            traj_columns, left_on="n", right_index=True, how="left"
        )
        self._make_dfs_categorical()

    def add_traj_cols_to_trajs_df(self, traj_columns: pd.DataFrame):
        """
        Adds information into self.trajs_df

        Args:
            traj_columns (pd.DataFrame): must contain "n" and "L" columns
        """
        assert "n" in traj_columns.columns
        assert "L" in traj_columns.columns
        for c in traj_columns:
            if c in self._trajs_df.columns and c not in ["n", "L"]:
                del self._trajs_df[c]
        assert "n" in self._trajs_df.columns, self._trajs_df.columns
        assert "L" in self._trajs_df.columns, self._trajs_df.columns
        if traj_columns.isnull().sum(axis=0).sum(axis=0) > 0:
            logging.debug("There are null values in the traj_columns")
            logging.debug(traj_columns.isnull().sum(axis=0))
        if self._trajs_df.isnull().sum(axis=0).sum(axis=0) > 0:
            logging.debug("There are null values in the actual trajs_df")
            logging.debug(self._trajs_df.isnull().sum(axis=0))
        self._trajs_df = self._trajs_df.merge(
            traj_columns, how="left", right_on=["n", "L"], left_on=["n", "L"]
        )
        null_rows = self._trajs_df.isnull().sum(axis=1) > 0
        if null_rows.sum() > 0:
            logging.warn(
                "%s :\tThere were null rows (%d / %d) in added dataframe"
                % (str(self), null_rows.sum(), null_rows.shape[0])
            )
        self._trajs_df = self._trajs_df.loc[~null_rows]
        self._make_dfs_categorical()
        self._save_trajs_df()

    def _make_dfs_categorical(self):
        if hasattr(self, "_trajs_df"):
            n_unique_values = self._trajs_df.nunique(axis=0)
            categorical_cols = n_unique_values.loc[
                n_unique_values <= ((self._trajs_df.shape[0] / 50) + 1)
            ].index.tolist()
            categorical_cols = [c for c in categorical_cols if c not in ["L","t","frame","n"]]
            self._trajs_df[categorical_cols] = self._trajs_df[categorical_cols].astype(
                "category"
            )
        if hasattr(self, "_locs"):
            n_unique_values = self._locs.nunique(axis=0)
            categorical_cols = n_unique_values.loc[
                n_unique_values <= ((self._locs.shape[0] / 50) + 1)
            ].index.tolist()
            categorical_cols = [c for c in categorical_cols if c not in ["L","t","frame","n"]]
            self._locs[categorical_cols] = self._locs[categorical_cols].astype(
                "category"
            )

    def trajectories_list(
        self,
        min_length: int = 7,
        return_indices: bool = True,
        filter: Callable = None,
    ):
        """
        Returns a list of trajectories whose length is above a given threshold, possibly filtered according to the locs they're based on

        Args:
            min_length (int, optional): Defaults to 7.
            return_indices (bool, optional): Whether to return indices ('n' column of the locs DataFrame) along with coordinates. Defaults to True.
            filter (Callable, optional): Callable, which takes as input the locs DataFrame and returns a boolean Series with the same index. Defaults to None.

        Returns:
            _type_: either a list of trajectories, or a tuple containing this same list and the list of indices
        """
        if filter is not None:
            df = pd.DataFrame(index=self.locs.index)
            df["n"] = self.locs.n
            df["cond"] = filter(self.locs)
            # The filter is applied point by point
            # Then we make sure to only select trajectories for which all points pass the filter
            cond = df.groupby("n")["cond"].min()
            cond = self.locs.n.isin(cond[cond].index.tolist())
        else:
            cond = ~self.locs.x.isnull()
        traj_length = self.locs.loc[cond].n.value_counts()
        good_trajs = traj_length.loc[traj_length >= min_length].index.tolist()
        indices = []
        trajs = []
        for n, traj in (
            self.locs.loc[self.locs.n.isin(good_trajs)]
            .sort_values(["n", "t"])
            .groupby("n")
        ):
            indices.append(n)
            trajs.append(traj[self.coord_cols].values)
        to_return = (trajs,)
        if return_indices:
            to_return = to_return + (indices,)
        return to_return

    def get_traj(self, n: int):
        return self.locs.loc[self.locs.n == n].sort_values("t")[self.coord_cols].values

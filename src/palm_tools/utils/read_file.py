import pandas as pd
from scipy.io import loadmat
import xml.etree.ElementTree as ET
import numpy as np
import logging
from scipy.stats import binomtest


def mapping_is_complete(mapping: dict, is3D: bool = False):
    for c in ["x", "y", "n"]:
        if c not in mapping:
            return False
    if ("t" not in mapping) and ("frame" not in mapping):
        return False
    if is3D and ("z" not in mapping):
        return False
    return True


def map_df_columns(df: pd.DataFrame, is3D: bool = False):

    mapping = {}

    standard_names = {
        "n": ["n", "traj", "trajectory"],
        "x": ["x"],
        "y": ["y"],
        "t": ["t", "time"],
        "frame": ["frame"],
    }
    if is3D:
        standard_names["z"] = ["z"]

    for col, candidates in standard_names.items():
        for c in candidates:
            if c in df.columns:
                mapping[col] = c
                break

    if not mapping_is_complete(mapping, is3D):

        index_col = None
        for c in df.columns:
            if df[c].is_monotonic_increasing and df[c].nunique() == df.shape[0]:
                index_col = c
                logging.debug("Index column : %s" % c)
                break
        if index_col is not None:
            df = df.set_index(index_col)
        sorted_columns = [c for c in df.columns if df[c].is_monotonic_increasing]
        logging.debug("There are sorted columns %s" % sorted_columns)

        # If there are several sorted columns, we only keep the ones which have
        # the greatest number of different values
        n_values = df[sorted_columns].nunique(axis=0)
        good_sorted_columns = n_values.loc[n_values == n_values.max()].index.tolist()

        logging.debug("There are candidate sorted columns %s" % good_sorted_columns)
        # print(df.head())
        for c in good_sorted_columns:
            if c in mapping:
                continue
            # is sorted column time or trajectory index ?
            sorted_counts = df[c].value_counts()
            median_count = sorted_counts.median()
            mean_count = sorted_counts.mean()
            # detections / frame is ~ normal (binomial)
            # => median / mean ~ 1
            # detections / trajectory is ~ exponential
            # => median / mean ~ ln(2)
            # print(median_count, mean_count)
            
            if (median_count / mean_count < (1.0 + np.log(2)) / 2):
                logging.debug("Sorted column is trajectory index")
                if "n" not in mapping:
                    logging.debug("%s is n" % c)
                    mapping["n"] = c
            else:
                logging.debug("Sorted column is time or frame")
                if "frame" not in mapping:
                    if (df[c].dtype == int) or (
                        (df[c].astype(int) - df[c]).abs().max() <= 0.001
                    ):  # This would work unless DT = 1.
                        logging.debug("%s is frame" % c)
                        mapping["frame"] = c
                if "t" not in mapping:
                    if df[c].dtype.kind == "f":
                        logging.debug("%s is time" % c)
                        mapping["t"] = c
        # Check which columns are significantly partially-sorted
        diff_columns = []
        diff_columns_p_vals = []

        for c in df.columns:
            if c in mapping:
                continue
            k = np.sum(df[c].iloc[1:].values > df[c].iloc[:-1].values)
            n = np.sum(df[c].iloc[1:].values != df[c].iloc[:-1].values)
            if n > 0:
                test = binomtest(k=k, n=n, p=0.5)
                pval = test.pvalue
                try:
                    stat = test.statistic
                except:
                    stat = test.proportion_estimate
                if (
                    (c not in sorted_columns)
                    and (pval < 0.01)
                    and (abs(stat - 0.5) > 0.05)
                ):
                    diff_columns.append(c)
                    diff_columns_p_vals.append(pval)

        logging.debug("Diff columns : %s" % diff_columns)

        # We only want to keep the maximally partially sorted columns,
        # which will be proportional to time and frame
        diff_columns = [
            c
            for c, p in zip(diff_columns, diff_columns_p_vals)
            if p == min(diff_columns_p_vals)
        ]

        if len(diff_columns) > 0:
            logging.debug("There are partially ordered columns")
            logging.debug(diff_columns)

        for c in diff_columns:
            if "n" in mapping:
                if (df[c].dtype == int) or (
                    (df[c].astype(int) - df[c]).abs().max() <= 0.001
                ):  # This would work unless DT = 1.
                    if "frame" not in mapping:
                        logging.debug("%s is frame" % c)
                        mapping["frame"] = c
                    else:
                        logging.debug("%s is not frame" % c)
                else:
                    if "t" not in mapping:
                        logging.debug("%s is time" % c)
                        mapping["t"] = c
                    else:
                        logging.debug("%s is not time" % c)
            elif "frame" in mapping or "t" in mapping:
                if (
                    df[c].dtype.kind in "fi"
                    and (df[c].astype(int) - df[c]).abs().max() == 0
                ):
                    mapping["n"] = c

    left_columns = [
        # Unassigned columns
        c
        for c in df.columns
        if c not in mapping.values() and df[c].dtype != int
    ]
    if (len(left_columns) >= 2) and ("x" not in mapping):
        mapping["x"] = left_columns[0]
        mapping["y"] = left_columns[1]
        if len(left_columns) >= 3 and ("z" not in mapping) and is3D:
            mapping["z"] = left_columns[2]

    if mapping_is_complete(mapping, is3D=is3D):
        inverse_mapping = {}
        for key, value in mapping.items():
            inverse_mapping[value] = key
        return inverse_mapping
    else:
        return None


def map_df_columns_with_names(df: pd.DataFrame, is3D: bool = False):
    """
    Returns a dict with name mappings towards x,y,t,frame,t,n

    Args:
        df (pd.DataFrame): _description_
    """
    # detect using names
    mapping = {}
    for c in df.columns:
        if str(c) != c:
            continue
        if c[0].lower() == "x" and "x" not in mapping:
            mapping["x"] = c
        elif c[0].lower() == "y" and "y" not in mapping:
            mapping["y"] = c
        elif is3D and c[0].lower() == "z" and "z" not in mapping:
            mapping["z"] = c
        elif c[0].lower() == "t" and len(c) == 1 and "t" not in mapping:
            mapping["t"] = c
        elif c[0].lower() == "n" and len(c) == 1 and "n" not in mapping:
            mapping["n"] = c
        elif c[:5].lower() == "frame" and "frame" not in mapping:
            mapping["frame"] = c
        elif "traj" in c.lower() and "n" not in mapping:
            mapping["n"] = c
    inverse_mapping = {}
    for key, value in mapping.items():
        inverse_mapping[value] = key

    if mapping_is_complete(mapping, is3D):
        return inverse_mapping
    else:
        return None


def load_csv_file(
    file_path: str,
    sep: str,
    DT: float = None,
    short_version: bool = False,
    is3D: bool = False,
):
    read_args = {"sep": sep} if sep != " " else {"delim_whitespace": True}

    # chunksize not to read the whole file -> it returns an iterator
    first_line = str(open(file_path, "r").readline())
    no_letters = first_line.lower() == first_line.upper()
    if no_letters:
        read_args["header"] = None
    for df_ in pd.read_csv(file_path, chunksize=50000, **read_args):
        df = df_.copy()
        break
    if len(df.columns) < 4:
        raise Exception("Not enough columns : %s" % (df.columns))  # x, y, n, t/frame

    col_mapping = map_df_columns(
        df, is3D=is3D
    )  # column names in loaded table -> rigid column names (n,x,y,...)
    

    if col_mapping is None:
        col_mapping = map_df_columns_with_names(df, is3D=is3D)
        if col_mapping is None:
            raise Exception("Could not detect columns")
        else:
            logging.info("Mapped columns")

    mapping = {v: k for k, v in col_mapping.items()}  # inverse mapping
    logging.info("Col mapping : %s" % mapping)
    assert mapping_is_complete(
        mapping, is3D=is3D
    ), "Not enough mapped columns %s " % (mapping)

    
    if not short_version:
        df = pd.read_csv(file_path, **read_args)
    df = df.rename(columns=col_mapping)
    logging.info("about to add t or frame")
    df, DT = add_t_or_frame(df, DT)
    return df, DT


def load_trackmate_file(file_path, is3D: bool = False):
    root_node = ET.parse(file_path).getroot()
    spots = []
    for spot in root_node.findall("Model/AllSpots/SpotsInFrame/Spot"):
        spots.append(spot.attrib)
    if len(spots) < 3:
        raise Exception("Less than 3 elements in tree")
    spots_df = pd.DataFrame.from_records(spots)
    spots_df["n"] = -1
    for i, track in enumerate(root_node.findall("Model/AllTracks/Track")):
        nodes = set([e.attrib["SPOT_SOURCE_ID"] for e in track.findall("Edge")])
        nodes = nodes.union([e.attrib["SPOT_TARGET_ID"] for e in track.findall("Edge")])
        spots_df.loc[spots_df["ID"].isin(nodes), "n"] = i
    spots_df = spots_df.loc[spots_df.n > -1]
    spots_df["ID"] = spots_df["ID"].astype(int)
    spots_df["POSITION_X"] = spots_df["POSITION_X"].astype(float)
    spots_df["POSITION_Y"] = spots_df["POSITION_Y"].astype(float)
    spots_df["POSITION_T"] = spots_df["POSITION_T"].astype(float)
    if is3D:
        spots_df["POSITION_Z"] = spots_df["POSITION_Z"].astype(float)
    spots_df["FRAME"] = spots_df["FRAME"].astype(int)
    spots_df = spots_df.rename(
        columns={
            "FRAME": "frame",
            "POSITION_T": "t",
            "POSITION_X": "x",
            "POSITION_Y": "y",
            "POSITION_Z": "z",
        }
    )
    DT = infer_DT(spots_df)
    return spots_df, DT


def flatten(x):
    """https://www.codecademy.com/forum_questions/52cc75f0631fe98640001510"""
    """ Creates a generator object that loops through a nested list """
    # First see if the list is iterable
    try:
        if isinstance(x, np.ndarray):
            raise TypeError()
        it_is = iter(x)
    # If it's not iterable return the list as is
    except TypeError:
        yield x
    # If it is iterable, loop through the list recursively
    else:
        for i in it_is:
            for j in flatten(i):
                yield j


def infer_DT(df: pd.DataFrame):
    logging.info("Infer_DT")
    DT = None
    for n, traj in df.sort_values("t").groupby("n"):
        if traj.shape[0] < 7:
            continue
        DT = np.min(traj["t"].values[1:] - traj["t"].values[:-1])
        break
    assert DT is not None, "DT is None"
    assert DT > 0.0, "DT is negative : %.2f" % DT
    DT = np.round(DT, 4)
    return DT


def add_t_or_frame(df: pd.DataFrame, DT: float = None):
    """Ensure that 't' AND 'frame' are present amongst columns.

    Args:
        df (pd.DataFrame): data frame whose columns must be completed
        DT (float, optional): time interval, in seconds, to be used when only the frame number is available.
        If it isn't indicated and DT cannot be inferred, it will be 1 second. Defaults to None.

    Returns:
        pd.DataFrame: data frame with added column
    """
    if "t" in df.columns and DT is None:
        DT = infer_DT(df)
    if ("t" in df.columns) and ("frame" in df.columns):
        return df, DT
    elif "t" in df.columns:
        df["frame"] = np.rint((df["t"].astype(float) / DT)).astype(int)
        if df.groupby(["n","frame"]).count().max().max() > 1:
            logging.critical("More than one position per n x frame. Keeping one arbitrarily.")
            df = df.groupby(["n","frame"]).first().reset_index()
        return df, DT
    elif "frame" in df.columns:
        logging.debug("Adding t column")
        DT = DT if DT is not None else 1.0
        df["t"] = df["frame"] * DT
        return df, DT


def check_file_and_read_(
    file_path: str, short_version: bool = False, DT: float = None, is3D: bool = True
):
    logging.debug("Opening %s" % file_path)

    error_message = ""
    # CSV with various separators
    try:
        possible_separators = [",", "\t", ";", " "]
        actual_separator = None
        with open(file_path, "r") as f:
            first_line = str(f.readline())
            for sep in possible_separators:
                if first_line.count(sep) >= 3:
                    actual_separator = sep
                    break
        if actual_separator is None:
            raise Exception("No separator in the first line : %s" % first_line)
        else:
            logging.debug("First line is %s" % first_line)
            logging.debug("Actual separator is %s" % actual_separator)
        df, DT = load_csv_file(
            file_path=file_path,
            sep=actual_separator,
            DT=DT,
            short_version=short_version,
            is3D=is3D,
        )
        df["n"] = df["n"].astype(int)
        return df, DT
    except Exception as e:
        logging.debug(e)
        logging.debug("this doesn't look like a tab-delimited file")
        error_message += "Couldn't read file as CSV. Try specifying explicit column names."

    # Matlab file
    try:
        mat = loadmat(file_path, struct_as_record=True, squeeze_me=True)
        try:
            mat_df = pd.DataFrame(data=mat)
            col_mapping = map_df_columns(mat_df, is3D=is3D)
            mat_df = mat_df.rename(columns=col_mapping)
            mat_df, DT = add_t_or_frame(mat_df, DT)
            mat_df["n"] = mat_df["n"].astype(int)
            return mat_df, DT
        except:
            for key, matrice in mat.items():
                if key[:2] == "__":
                    continue
                # logging.info(matrice[0,0])
                if not isinstance(matrice, np.ndarray):
                    continue
                logging.info("%s is an array" % key)
                logging.info(matrice.dtype)
                logging.info(matrice.shape)
                if matrice.dtype == np.ndarray:
                    mats = []
                    for n, mat in enumerate(matrice):
                        mat_ = np.concatenate(
                            [np.reshape(np.ones(mat.shape[0]), (-1, 1)) * n, mat],
                            axis=1,
                        )
                        mats.append(mat_)
                    matrice = np.concatenate(mats, axis=0)
                logging.info(matrice.dtype)
                logging.info(type(matrice))
                logging.info(matrice.shape)
                if not len(matrice.shape) == 2:
                    continue
                if not matrice.shape[1] >= 4:
                    continue
                try:
                    mat_df = pd.DataFrame(data=matrice)
                    col_mapping = map_df_columns(mat_df, is3D=is3D)
                    logging.info("col_mapping: %s" % col_mapping)
                    mat_df = mat_df.rename(columns=col_mapping)
                    mat_df, DT = add_t_or_frame(mat_df, DT)
                    mat_df["n"] = mat_df["n"].astype(int)
                    return mat_df, DT
                except Exception as e:
                    logging.debug(e)
                    logging.debug("Matlab matrice %s is not a valid one" % key)
            raise
    except Exception as e:
        logging.debug("Error testing mat file")
        logging.debug(e)
        error_message += "Couldn't read as matlab file\n"
        # error_message += "\t %s\n" % e.__str__

    # trackmate file
    try:
        trackmate_file, DT = load_trackmate_file(file_path, is3D=is3D)
        return trackmate_file, DT
    except Exception as e:
        logging.debug("Error in opening Trackmate file")
        logging.debug(e)
        error_message += "Couldn't read as Trackmate file\n"
        # error_message += "\t %s\n" % e.__str__

    return None, error_message


def check_file_and_read(
    file_path: str,
    short_version: bool = False,
    check_min_trajs: int = 0,
    cut_trajs_longer_than: int = 0,
    reject_trajs_shorter_than: int = 7,
    DT: float = None,
    is3D: bool = False,
):
    """
    Checks whether a file contains trajectories or not.
    Returns a tuple df, DT
    where df is a pd.DataFrame and DT is the exposure (in s.)
    if there is an error, df is None and DT is an error message
    if check_min_trajs > 0, it returns an err

    Args:
        file_path (str): path of the file to read
    """
    df, DT = check_file_and_read_(
        file_path=file_path, short_version=short_version, DT=DT, is3D=is3D
    )
    if df is not None:

        df[["x", "y", "t"]] = df[["x", "y", "t"]].astype(float)
        if is3D:
            df["z"] = df["z"].astype(float)
        df["frame"] = df["frame"].astype(int)
        df["n"] = df["n"].astype(int)

        if reject_trajs_shorter_than > 0 or check_min_trajs > 0 or cut_trajs_longer_than > 0:
            lengths = df.n.value_counts()
            
        if reject_trajs_shorter_than > 0:
            df = df.loc[df.n.isin(lengths.loc[lengths >= reject_trajs_shorter_than].index.tolist())]
            
        if check_min_trajs > 0:
            if reject_trajs_shorter_than > 0:    
                n_valid_trajs = (lengths >= reject_trajs_shorter_than).sum()
                if n_valid_trajs < check_min_trajs:
                    df = None
                    DT = (
                        "There were too few (%d < %d) trajectories of length >= %d" % (n_valid_trajs, check_min_trajs, reject_trajs_shorter_than)
                        % n_valid_trajs
                    )  
            else:
                logging.warn("Should specify reject_trajs_shorter_than: int > 0 to check the minimum length of trajectories")
                
        if cut_trajs_longer_than > 0:
            assert cut_trajs_longer_than >= reject_trajs_shorter_than, "Invalid combination of arguments"
            logging.info("%d trajectories are longer than %d and will be cut" % ((lengths > cut_trajs_longer_than).sum(), cut_trajs_longer_than))
            df = df.sort_values("t").groupby("n").apply(lambda t: t[[c for c in t.columns if c != "n"]].iloc[:min(t.shape[0],cut_trajs_longer_than)]).reset_index()
            del df["level_1"]

    return df, DT

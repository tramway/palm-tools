from itertools import combinations, product
import itertools
import json
from logging import critical
from multiprocessing import Pool
import os
from typing import Any, Callable, Dict, Iterable, List, Tuple, Union

import logging
from matplotlib.colors import TwoSlopeNorm
import matplotlib.gridspec as gridspec
from palm_tools.data_structure.trackset import TrackSet, TrackSets
from .analysis_classes import AnalysisStep
from .analysis_tools.mmd import (
    MMD2,
    get_sigma,
    witness_function,
    keep_one_length_per_traj,
)
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.ndimage import maximum_position
from scipy.special import comb
from scipy.stats import pearsonr, binomtest
import scipy
import zarr
import matplotlib.ticker as mtick
import warnings

# MAX_N_SAMPLES_IN_FILE = 3000


def null_filter_func(df):
    return ~df.n.isnull()


class LatentVecsBasedAnalysisStep(AnalysisStep):
    def __init__(
        self,
        track_sets: TrackSets,
        run_name: str,
        n_max_trajs_per_unit: int = 1000,
        n_min_trajs_per_unit: int = 150,
        filter_func: Callable = null_filter_func,
        # filter considered conditions {"key":["possible_value_1","possible_value_2",...]}
        cond_filter: Dict = None,
        locs_cols_to_keep: Dict = {},
        # key = the column name, value = the method to aggregate values from a trajectory
        n_processes: int = 8,
        latent_dim: int = 16,
        unbiased: bool = True,
        latent_cols: List = None,
        use_high_dimension: bool = False,
        show_progress: bool = False,
        unit_key: List[str] = ["file"],  # what defines a unit (a list of keys)
        logger: logging.Logger = None
    ):
        
        super(LatentVecsBasedAnalysisStep, self).__init__(
            tracksets=track_sets, run_name=run_name, logger=logger,
        )

        self.filter_func = filter_func
        self.use_high_dimension = use_high_dimension
        self.n_max_trajs_per_unit = n_max_trajs_per_unit
        self.n_min_trajs_per_unit = n_min_trajs_per_unit
        self.cond_filter = cond_filter
        self.n_processes = n_processes
        self.unit_key = unit_key
        self.unbiased = unbiased
        self.locs_cols_to_keep = locs_cols_to_keep
        self.latent_dim = latent_dim
        self.show_progress = show_progress
        if latent_cols is not None:
            self._latent_cols = latent_cols
        self.coord_cols = track_sets.coord_cols

    def params_dict(self) -> dict:
        d = super().params_dict()
        d["n_max_trajs_per_unit"] = self.n_max_trajs_per_unit
        d["n_min_trajs_per_unit"] = self.n_min_trajs_per_unit
        # for now, do not support filter_func
        d["cond_filter"] = self.cond_filter
        d["n_processes"] = self.n_processes
        d["unit_key"] = self.unit_key
        d["unbiased"] = self.unbiased
        d["locs_cols_to_keep"] = self.locs_cols_to_keep
        d["latent_dim"] = self.latent_dim
        d["show_progress"] = self.show_progress
        d["latent_cols"] = self.latent_cols
        return d

    def _add_trackset(args) -> pd.DataFrame:
        """
        Extract latent vectors from trajectories in a trackset
        select only those passing the filter_func
        mark columns according to things defining the units, but doesn't group by units yet
        """
        self, ts = args
        self: LatentVecsBasedAnalysisStep = self
        ts: TrackSet = ts
        # print("Processing %s" % ts.origin_file)
        # _, good_trajs = ts.trajectories_list(
        #    return_indices=True, filter=self.filter_func
        # )
        n_score = ts.locs.groupby("n").apply(lambda df: self.filter_func(df).mean())
        good_trajs = n_score.loc[n_score > 0.5].index.unique().tolist()
        # print("Adding trackset %s" % ts)
        try:
            if len(good_trajs) <= self.n_min_trajs_per_unit:
                self.logger.info(
                    "Not including %s because too few valid rows were found"
                    % ts.origin_file
                )
                return None

            else:
                # print("%d good trajs in %s" % (len(good_trajs), ts.origin_file))
                # We add eventual columns which would only be present in the locs table and not in the trajs_df one.
                cols_to_keep = [c for c in ts.locs.columns if c not in ts.trajs_df] + [
                    "n"
                ]
                cols_to_keep = cols_to_keep + [
                    colname
                    for colname, agg_method in self.locs_cols_to_keep.items()
                    if colname not in cols_to_keep
                ]
                agg_dict = {}
                for c in cols_to_keep:
                    if c == "n":
                        continue
                    if c in self.locs_cols_to_keep:
                        agg_dict[c] = self.locs_cols_to_keep[c]
                    else:
                        agg_dict[c] = "first"
                trajs_df = ts.trajs_df

                df = trajs_df.merge(
                    ts.locs[cols_to_keep].groupby("n").agg(agg_dict).reset_index(),
                    how="left",
                    left_on="n",
                    right_on="n",
                )
                # assert df.groupby(["L", "n"]).count().max().max() <= 1
                full_rows = df.isnull().sum(axis=1) == 0
                self.logger.debug(
                    "%d / %d full rows" % (full_rows.sum(), full_rows.shape[0])
                )
                df = df.loc[full_rows]
                df["file"] = ts.origin_file
                df["file"] = df["file"].astype("category")
                index_df = self.tracksets.index_df
                index_row = index_df.loc[index_df.file == ts.origin_file]
                assert index_row.shape[0] == 1
                for i, row in index_row.iterrows():
                    for key, value in row.iteritems():
                        df[key] = value
                        df[key] = df[key].astype("category")
                # print("About to return")
                return (df, ts.origin_file)

        except KeyError as e:
            self.logger.error("Missing columns for %s : %s" % (ts.origin_file, e))
        except BaseException as e:
            raise

    @property
    def units(self) -> dict:
        """
        {1:("cond_a","type_x),2:("cond_b","type_z"),...}
        """
        if not hasattr(self, "_units"):
            self.latent_df
        return self._units

    @property
    def latent_df(self) -> pd.DataFrame:
        """
        Returns a dataframe with latent vectors and unit ID
        Populates self.units, which maps group_IDs to conditions
        latent_df is build from a concateation of ts.trajs_df
        """
        if not hasattr(self, "_latent_df"):
            tracksets = self.tracksets
            index_df = self.tracksets.index_df
            valid_file_cond = ~index_df["file"].isnull()
            if self.cond_filter is not None:
                for key, valid_values in self.cond_filter.items():
                    if key not in index_df.columns:
                        self.logger.warning(
                            "Key %s is not an index_df column, skipping filter" % key
                        )
                        continue
                    valid_file_cond = valid_file_cond & index_df[key].isin(valid_values)
            files_to_keep = index_df.loc[valid_file_cond].file.unique().tolist()

            tracksets = [ts for ts in tracksets if ts.origin_file in files_to_keep]

            p = Pool(self.n_processes)

            args = [(self, ts) for ts in tracksets]
            # args = args[:4]  # pour tests
            test = self.n_processes <= 1
            if test:
                try:
                    LatentVecsBasedAnalysisStep._add_trackset(args[0])
                except Exception as e:
                    raise e
            pool_result = p.imap_unordered(
                LatentVecsBasedAnalysisStep._add_trackset, args
            )
            tqdm_ = tqdm(
                pool_result,
                total=int(len(args)),
                desc="Adding units from tracksets",
                disable=(len(args) < 20) | ~self.show_progress,
            )

            to_concat = []

            for returned in tqdm_:
                if returned is None:
                    continue
                df, origin_file = returned
                assert df.groupby(["n", "L"]).count().max().max() == 1
                float_cols = list(df.select_dtypes(include=["f", float]).columns)

                to_concat.append(df)
                tqdm_.set_postfix_str(
                    "%d trajs from %s" % (df.n.nunique(), origin_file)
                )

            p.close()
            p.join()

            to_concat_filtered = []
            self._units = {}
            unit_ID = 0
            for df in to_concat:
                for g, df_ in df.groupby(self.unit_key):
                    if len(self.unit_key) == 1:
                        g = (g,)
                    if df_.n.nunique() >= self.n_min_trajs_per_unit:
                        df_c = df_.copy()
                        df_c["unit"] = unit_ID
                        df_c["unit"] = df_c["unit"].astype("category")
                        self._units[unit_ID] = {}
                        for key, value in zip(self.unit_key, g):
                            self._units[unit_ID][key] = value
                        unit_ID += 1
                        to_concat_filtered.append(df_c)
                    else:
                        self.logger.debug(
                            "Not enough trajectories in unit %s, there are only %d"
                            % (g, df_.n.nunique())
                        )
            self._latent_df = pd.concat(to_concat_filtered)

            unique_values = self._latent_df.nunique(axis=0)
            categorical_columns = unique_values.loc[
                (unique_values <= max(10, (self._latent_df.shape[0] / 50) + 1))
            ].index.tolist()
            categorical_columns = [
                c for c in categorical_columns if c not in float_cols
            ]
            categorical_columns = [c for c in categorical_columns if c != "L"]
            self._latent_df[categorical_columns] = self._latent_df[
                categorical_columns
            ].astype("category")
            # print("Categorical :")
            # print(categorical_columns)

            #
            traj_ID = (
                self._latent_df.groupby(["file", "n"])[["file"]]
                .first()
                .rename(columns={"file": "traj_ID"})
            )
            traj_ID["traj_ID"] = np.arange(traj_ID.shape[0])
            self._latent_df = self._latent_df.merge(
                traj_ID[["traj_ID"]],
                how="left",
                left_on=["file", "n"],
                right_index=True,
            )
            # self._latent_df["traj_ID"] = (
            #    self._latent_df["n"].astype(str)
            #    + " "
            #    + self._latent_df["file"].astype(str)
            # )
            # IDs = self._latent_df["traj_ID"].unique()
            # IDs_dict = dict(zip(IDs, range(len(IDs))))
            # self._latent_df["traj_ID"].replace(IDs_dict, inplace=True)

            assert self._latent_df["unit"].nunique() == len(to_concat_filtered)
            assert len(to_concat_filtered) == len(self._units)
            # print("latent_df has %d units" % self._latent_df["unit"].nunique())

        return self._latent_df

    @property
    def latent_cols(self) -> dict:
        if not hasattr(self, "_latent_cols"):
            if not self.use_high_dimension:
                self._latent_cols = ["U_1", "U_2"]
            else:
                # Use high-dimensional latent space
                self._latent_cols = ["h_%d" % (i + 1) for i in range(self.latent_dim)]
        return self._latent_cols

    @property
    def latent_vecs(self) -> dict:
        if not hasattr(self, "_latent_vecs"):
            self._latent_vecs = {}
            for g, df_ in self.latent_df.groupby("unit"):
                self._latent_vecs[g] = df_[
                    self.latent_cols + ["n", "L", "traj_ID"]
                ].copy()
        return self._latent_vecs

    @property
    def global_sigma(self) -> float:
        if not hasattr(self, "_global_sigma"):
            H = self.latent_df[self.latent_cols].sample(3000, replace=True).values
            self._global_sigma = get_sigma(H, H)
        return self._global_sigma

    def _save(self):
        self.latent_df.to_csv(
            os.path.join(self.output_path, "latent_df.csv"), index=True
        )
        json.dump(self.units, open(os.path.join(self.output_path, "units.json"), "w"))

    def _load_results(self):
        super()._load_results()
        self._latent_df = pd.read_csv(os.path.join(self.output_path, "latent_df.csv"), keep_default_na=False, index_col=0)
        self._latent_df["unit"] = self._latent_df["unit"].astype(int)
        self._units = json.load(open(os.path.join(self.output_path, "units.json"), "r"))


class GroupBasedAnalysisStep(LatentVecsBasedAnalysisStep):
    def __init__(
        self,
        group_by_keys: Union[
            str, List[str]
        ],  # group files by these keys (columns of exp.index_df or acq.locs)
        # {"type":"cellule du foie"}
        # {"type":"cellule du poumon"} # granularité = 1
        # {"type":"cellule du poumon","temps":"à jeun"} # granularité = 2
        n_max_bootstraps: int = 50,
        min_granularity: int = 1,  # consider groups defined by at least n keys.
        compare_groups_with_self: bool = False,
        hierarchy: Dict = {},
        **kwargs
    ):
        
        if isinstance(group_by_keys, str):
            self.group_by_keys = [group_by_keys]
        else:
            self.group_by_keys = group_by_keys
        
        self.hierarchy = hierarchy
        if len(hierarchy) == 0:
            # By default, put all factors at the top level
            for key in self.group_by_keys:
                self.hierarchy[key] = 0
                
        self.n_max_bootstraps = n_max_bootstraps
        assert all([key in self.hierarchy for key in self.group_by_keys])
            
        self.min_granularity = max(1, min(min_granularity, len(self.group_by_keys)))
        self.latent_vecs_dict = {}  # To be used in get_closest_trajectories
        self.compare_groups_with_self = compare_groups_with_self
        super(GroupBasedAnalysisStep, self).__init__(**kwargs)
        
        self.tracksets.index_df[self.group_by_keys] = self.tracksets.index_df[self.group_by_keys].astype(str)

    @property
    def latent_df(self) -> pd.DataFrame:
        if not hasattr(self, "_latent_df"):
            self._latent_df = super().latent_df
            self._latent_df[self.group_by_keys] = self._latent_df[self.group_by_keys].astype(str)
        return self._latent_df

    def params_dict(self) -> dict:
        d = super().params_dict()
        d["group_by_keys"] = self.group_by_keys
        d["n_max_bootstraps"] = self.n_max_bootstraps
        d["min_granularity"] = self.min_granularity
        d["compare_groups_with_self"] = self.compare_groups_with_self
        d["hierarchy"] = self.hierarchy 
        return d
    
    def _save(self):
        super()._save()
        json.dump(self.groups, fp=open(os.path.join(self.output_path, "groups.json"), "w"))
        json.dump(self.comparisons, fp=open(os.path.join(self.output_path, "comparisons.json"), "w"))
        
    def _load_results(self):
        super()._load_results()
        self._groups = json.load(open(os.path.join(self.output_path, "groups.json"), "r"))
        self._comparisons = json.load(open(os.path.join(self.output_path, "comparisons.json"), "r"))
        self._latent_df[self.group_by_keys] = self._latent_df[self.group_by_keys].astype(str)

    @property
    def units_per_condition(self) -> Dict:
        """
        Returns {"cond_type_x":{"value1":[unit1,unit2,...],"value2":[unit4,unit1,...]},
                "cond_type_y":...}
        """
        if not hasattr(self, "_units_per_condition"):
            self._units_per_condition = {}
            for column in self.group_by_keys:
                self._units_per_condition[column] = {}
                for v, df in self.latent_df.groupby(column):
                    self._units_per_condition[column][v] = (
                        df["unit"].astype(int).unique().tolist()
                    )
        return self._units_per_condition

    def get_units_for_condition(self, condition: dict) -> List[str]:
        """
        Returns the list of units which are in a condition defined by several keys / values.
        Returns only units which have sufficiently enough trajectories, i.e. which are in self.latent_vecs
        (e.g. cell observed at t = t_i AND treated with X)
        """
        units = [int(u) for u in list(self.units.keys())]
        for key, value in condition.items():
            assert key in self.group_by_keys
            units = [
                int(f) for f in units if int(f) in self.units_per_condition[key][value]
            ]

        return units
    
    @property
    def valid_group_by_keys(self) -> List:
        if not hasattr(self, "_valid_group_by_keys"):
            # group_by_keys = ["activation","cell_type"]
            valid_group_by_keys = self.latent_df[self.group_by_keys].nunique(axis=0)
            valid_group_by_keys = valid_group_by_keys.loc[
                valid_group_by_keys > 1
            ].index.tolist()
            self._valid_group_by_keys = valid_group_by_keys
            # logging.warn("Valid group by keys")
            # logging.warn(self._valid_group_by_keys)
        assert all([key in self.group_by_keys for key in self._valid_group_by_keys])
        return self._valid_group_by_keys

    @property
    def groups(self) -> List[dict]:
        """
        A group is a dict with at most as many keys as there are elements in self.all_group_by_keys
        """
        if not hasattr(self, "_groups"):
            key_sets = []
            
            for l in range(self.min_granularity - 1, len(self.valid_group_by_keys)):
                key_sets += list(combinations(self.valid_group_by_keys, l + 1))
            # key_sets = [("activation"),("activation","cell_type"), ("cell_type")]
            
            self._groups = [] # Will be the returned list
            group_units = [] # List of sets of units. Will be used so as not to consider two groups referring to the exact same sets of units
            hierarchy_levels = self.hierarchy.values()
            
            for key_set in key_sets:
                
                # On ne garde que les groupes qui respectent la hiérarchie (c.à.d que la présence des facteurs doit respecter l'ordre hiérarchique)
                group_hierarchy_levels = set([self.hierarchy[key] for key in key_set])
                missing_hierarchy_levels = [l for l in hierarchy_levels if l not in group_hierarchy_levels]
                if len(missing_hierarchy_levels) > 0 and min(missing_hierarchy_levels) < max(group_hierarchy_levels):
                    continue
                # Si l'on va jusqu'à un niveau de détail, il faut que tous les niveaux supérieurs soient spécifiés
                if any([k not in key_set for k in self.hierarchy.keys() if self.hierarchy[k] < max(group_hierarchy_levels)]):
                    continue
                
                # key_set = ("activation","cell_type")
                possible_values = [
                    v
                    for v in product(
                        *[self.units_per_condition[key].keys() for key in key_set]
                    )
                ]
                # S'il n'y a qu'une valeur possible, pas la peine de rajouter des groupes :
                # par ex, s'il y a une colonne du fichier index pour laquelle toutes les lignes ont la même valeur, il ne faut pas s'en servir dans les groupes.
                if len(possible_values) <= 1:
                    self.logger.critical(
                        "Only one value : %s, discarding key set %s"
                        % (possible_values, key_set)
                    )
                    continue
                for values in possible_values:
                    # values = ["activated", "CD4+ cell"]
                    group = {}
                    
                    for key, value in zip(key_set, values):
                        group[key] = value
                        
                    units = self.get_units_for_condition(group)
                    if len(units) > 0:
                        self._groups.append(group)
                        group_units.append(frozenset(units))
            
            group_units = set(group_units)
            self.logger.debug("%d groups to compare, %d in reality (because some might be equivalent)" % (len(self._groups), len(group_units)))
            self.logger.debug(self._groups)
            
            
        return self._groups

    @property
    def n_groups(self):
        # Just in case we change the grouping strategy / structure
        return len(self.groups)

    @property
    def comparisons(self):
        # [(condA,condB),(condA,condC),...]
        if not hasattr(self, "_comparisons"):
            candidate_comparisons = list(combinations(self.groups, 2))
            self._comparisons = []
            for c1, c2 in candidate_comparisons:
                nkeys1, nkeys2 = len(c1.keys()), len(c2.keys())
                keys_in_common = set(c1.keys()).intersection(set(c2.keys()))
                if len(keys_in_common) == 0:
                    # Groups must be defined at least by one same key (=type of criteria)
                    continue
                if nkeys1 != nkeys2:
                    # We only consider comparisons defined by the same number of keys
                    continue
                
                min_hierarchy_of_differing_keys = np.inf
                max_hierarchy_of_differing_keys = -np.inf
                max_hierarchy_of_keys = max([self.hierarchy[key] for key in keys_in_common])
                for key in keys_in_common:
                    if c1[key] != c2[key]:
                        min_hierarchy_of_differing_keys = min(min_hierarchy_of_differing_keys,self.hierarchy[key])
                        max_hierarchy_of_differing_keys = max(max_hierarchy_of_differing_keys,self.hierarchy[key])
                
                
                
                if (min_hierarchy_of_differing_keys == max_hierarchy_of_differing_keys) and (max_hierarchy_of_keys == min_hierarchy_of_differing_keys):
                    self.comparisons.append((c1,c2))
                    
            if self.compare_groups_with_self:
                for c in self.groups:
                    self._comparisons.append((c, c))

            self.logger.info("%d comparisons to make" % (len(self._comparisons)))
            for c1, c2 in self._comparisons:
                self.logger.debug("%s | %s" % ("/".join([str(v) for v in c1.values()]),"/".join([str(v) for v in c2.values()])))

        return self._comparisons
    
    @property
    def equivalent_comparisons(self) -> Dict:
        if not hasattr(self, "_equivalent_comparisons"):
            self._equivalent_comparisons = {}
            for comp in self.comparisons:
                s = self._comparison_hash(comp)
                if s not in self._equivalent_comparisons:
                    self._equivalent_comparisons[s] = [comp]
                else:
                    self.logger.info("%s is equivalent to %s" % (comp,self._equivalent_comparisons[s][0]))
                    self._equivalent_comparisons[s].append(comp)
            if len(self._equivalent_comparisons) < len(self.comparisons):
                self.logger.info("In reality, %d comparisons to make (some of the %d are equivalent)" % (len(self._equivalent_comparisons), len(self.comparisons)))
        return self._equivalent_comparisons
    
    def _comparison_hash(self, comp):
        """
        Returns a hash which identifies each comparison (no matter the order of the tuple)
        """
        cond1, cond2 = comp
        units1 = frozenset(self.get_units_for_condition(cond1))
        units2 = frozenset(self.get_units_for_condition(cond2))
        s = hash(frozenset(sorted([hash(units1),hash(units2)])))
        return s

    def sample_latent_vecs_for_cond(
        self, cond: dict, return_units: bool = False, max_n_trajs_per_sample: int = None
    ):
        """
        To be used only in plotting functions, mot to generates tables used in the MMD
        """
        units = self.get_units_for_condition(cond)
        n_samples = min([self.latent_vecs[u].traj_ID.nunique() for u in units])
        if max_n_trajs_per_sample is not None:
            n_samples = min(n_samples, max_n_trajs_per_sample)
        return self._concat_equal_length_latent_dfs(
            units, n_samples_per_unit=n_samples, return_units=return_units
        )

    def _concat_equal_length_latent_dfs(
        self, units: List, n_samples_per_unit: int, return_units: bool = False
    ):
        """
        Returns a concatenation of latent vectors,
        sampled with an equal number of trajectories across all units
        note that there can be several rows per trajectory, as there is one row per trajectrory x length
        """
        if len(units) == 0:
            raise "Should not call concat with 0 units"
        unit_latent_vecs = []
        units_labels = []

        for u in units:
            v = self.latent_vecs[u]
            n_to_pick = np.random.choice(
                v.n.unique().tolist(), n_samples_per_unit, replace=False
            )
            unit_latent_vecs.append(v.loc[v.n.isin(n_to_pick)])
            units_labels += [u] * int(v.n.isin(n_to_pick).sum())
        to_return = pd.concat(
            unit_latent_vecs,
            axis=0,
        )
        if return_units:
            return to_return, units_labels
        else:
            return to_return

    @property
    def lims(self):
        if not hasattr(self, "_lims"):
            # self.logger.info("Getting limits")
            H = self.sample_latent_vecs_for_cond({}, max_n_trajs_per_sample=500)
            H, _ = keep_one_length_per_traj([H, H], cols_to_keep=self.latent_cols)
            if H.shape[0] > 3000:
                H = H.sample(3000, replace=False)
            self._global_sigma = get_sigma(H, H)
            xmin = np.quantile(H.values[:, 0], 0.005)
            xmax = np.quantile(H.values[:, 0], 0.995)
            if len(self.latent_cols) >= 2:
                ymin = np.quantile(H.values[:, 1], 0.005)
                ymax = np.quantile(H.values[:, 1], 0.995)
            else:
                ymin, ymax = xmin, xmax
            self._lims = xmin, xmax, ymin, ymax
            self._global_H = H
        return self._lims

    @property
    def global_H(self):
        if not hasattr(self, "_global_H"):
            self.lims
        return self._global_H

    @property
    def global_sigma(self):
        if not hasattr(self, "_global_sigma"):
            self.lims
        return self._global_sigma

    def find_closest_trajectories(
        self,
        latent_pos: np.ndarray,
        cond: dict = {},
        n_trajs: int = 9,
        min_length: int = None,
        max_length: int = None,
        max_dist: float = np.inf,
    ):
        cond_label = ",".join([v for key, v in cond.items()])
        if cond_label not in self.latent_vecs_dict:
            units = self.get_units_for_condition(cond)
            dfs = []
            for u in units:
                # latent_cols = [c for c in self.latent_vecs[u].columns]
                df = self.latent_vecs[u].copy().reset_index()
                df["unit"] = u
                df["file"] = self.latent_df.loc[
                    self.latent_df.unit == u, "file"
                ].values[0]
                dfs.append(df)
            df = pd.concat(dfs, axis=0)
            self.latent_vecs_dict[cond_label] = df
        else:
            df = self.latent_vecs_dict[cond_label].copy()

        df["distance"] = np.linalg.norm(
            df[self.latent_cols].values - latent_pos, axis=1
        )
        df = df.loc[df["distance"] < max_dist]
        df.sort_values("distance", inplace=True)
        df = df.groupby("traj_ID").first().reset_index()
        df.sort_values("distance", inplace=True)

        trajs = []
        for i, row in df.iterrows():
            ts = [ts for ts in self.tracksets if ts.origin_file == row["file"]][0]
            traj = ts.get_traj(row["n"])
            if min_length is None or traj.shape[0] >= min_length:
                if max_length is None or traj.shape[0] <= max_length:
                    trajs.append(traj)
            if len(trajs) >= n_trajs:
                break
        return trajs

    ### PLOT ###

    def plot_MMD_histogram(
        self,
        cond1,
        cond2,
        fig_path: str = None,
        close: bool = True,
        hide_xticks: bool = False,
    ):
        assert ((cond1, cond2) in self.comparisons) or (
            (cond2, cond1) in self.comparisons
        ), "This is not a valid comparison to query"
        index_1 = -1
        index_2 = -1
        for i, g in enumerate(self.groups):
            if g == cond1:
                index_1 = i
                continue
            if g == cond2:
                index_2 = i

        fig = plt.figure(figsize=(5, 4))
        ax = fig.add_subplot(111)
        ax.hist(
            self._D_null[index_1, index_2],
            alpha=0.5,
            label="$MMD_u^2[\mathcal{F},X',Y']$\n %d random splits"
            % self.n_max_bootstraps,
        )
        ax.axvline(
            np.quantile(self._D_null[index_1, index_2], 0.95),
            c="green",
            label="top $5\%$",
        )
        ax.axvline(
            np.quantile(self._D_null[index_1, index_2], 0.99),
            c="orange",
            label="top $1\%$",
        )
        ax.axvline(
            self._D_true[index_1, index_2], c="red", label="$MMD_u^2[\mathcal{F},X,Y]$"
        )
        # ax.set_title("%s %s VS\n%s %s" % (cond1 + cond2))

        plt.yticks([])

        def fmt_kernel(x, pos):
            try:
                e = int(np.floor(np.log10(np.abs(x))))
                xs = x / np.power(10.0, e)
                s = "$%d\\times10^{%d}$" % (xs, e)
                return s
            except:
                return "$0$"

        ax.xaxis.set_major_formatter(fmt_kernel)
        if hide_xticks:
            plt.xticks([])
        else:
            plt.xlabel("$MMD_u^2$")
            plt.legend()

        if fig_path is not None:
            plt.savefig(fig_path)
        if close:
            plt.close()

    def plot_witness_function(
        self,
        cond1: Dict[str, Any],
        cond2: Dict[str, Any],
        lims=None,
        fig_path: str = None,
        close: bool = False,
        max_abs: float = None,
    ):

        assert not self.use_high_dimension, "Only available for 2D latent vectors"
        H1 = self.sample_latent_vecs_for_cond(cond1, max_n_trajs_per_sample=2000)
        H2 = self.sample_latent_vecs_for_cond(cond2, max_n_trajs_per_sample=2000)
        H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)

        if lims is None:
            lims = self.lims
        xx, yy, w, w1, w2 = witness_function(H1.values, H2.values, lims=lims)

        dnorm = TwoSlopeNorm(
            vcenter=0,
            vmin=np.min(w) if max_abs is None else -max_abs,
            vmax=np.max(w) if max_abs is None else max_abs,
        )

        fig = plt.figure(figsize=(4, 4))

        ax = fig.add_subplot(111)
        cm = ax.contourf(xx, yy, w, norm=dnorm, cmap="Spectral", levels=50)
        self._add_grid(ax)
        # l_cond1 = "\n".join([str(v) for key, v in cond1.items()])
        # l_cond2 = "\n".join([str(v) for key, v in cond2.items()])
        # cond1 est en rouge (à double-vérifier)
        # plt.title("%s VS %s" % (l_cond1, l_cond2))
        # label="%s    <---->    %s" % (l_cond2, l_cond1)

        # plt.colorbar(cm)
        plt.tight_layout()
        if fig_path is not None:
            plt.savefig(fig_path)
        if close:
            plt.close()

    def _add_grid(self, ax, hide_ticks: bool = True):
        xticks = [
            int(x // 5) * 5
            for x in np.linspace(self.lims[0], self.lims[1], 30)
            if int(x // 5) * 5 >= self.lims[0] and int(x // 5) * 5 <= self.lims[1]
        ]
        yticks = [
            int(x // 5) * 5
            for x in np.linspace(self.lims[2], self.lims[3], 30)
            if int(x // 5) * 5 >= self.lims[2] and int(x // 5) * 5 <= self.lims[3]
        ]
        if hide_ticks:
            plt.xticks(xticks, labels=["" for x in xticks])
            plt.yticks(yticks, labels=["" for y in yticks])
        plt.grid(True)
        ax.xaxis.set_tick_params(size=0)
        ax.yaxis.set_tick_params(size=0)
        ax.set_aspect("equal")

    def plot_density(
        self, cond, cmap="Blues", fig_path: str = None, close: bool = False
    ):
        H = self.sample_latent_vecs_for_cond(cond, max_n_trajs_per_sample=2000)
        H, _ = keep_one_length_per_traj([H, H], cols_to_keep=self.latent_cols)
        xx, yy, _, w, _ = witness_function(
            H.values,
            H.values,
            lims=self.lims,
            sigma=self.global_sigma,
        )
        fig = plt.figure(figsize=(4, 4))
        ax = fig.add_subplot(111)
        ax.contourf(xx, yy, w, cmap=cmap, levels=50)
        self._add_grid(ax)
        plt.tight_layout()
        if fig_path is not None:
            plt.savefig(fig_path)
        if close:
            plt.close()

    def plot_latent_spaces(
        self,
        groups: Dict,  # {"activation":["little","big"],"cell_type":["type_1","type_2"]}
        main_condition: Dict = {},
        show_files: bool = True,
        show_scatter: bool = False,  # Show scatter plots instead of densities
        max_trajs_per_plot: int = 3000,
    ):
        """
        Plots the latent densities of sub_conditions inside main_condition.
        sub_conditions are defined by `groups`
        if show_files, display the latent densities of each ROI on the side
        if show_scatter, display scatterplots of latent vectors instead of densities
        """
        assert len(groups) <= 2
        assert all([key in self.group_by_keys for key in groups])
        assert all([key not in main_condition for key in groups])
        sub_conditions = {}
        keys = [k for k in groups]
        key1 = keys[0]
        key2 = keys[1] if len(keys) == 2 else None
        values_1 = groups[key1]
        values_2 = groups[key2] if key2 is not None else []
        for v1 in values_1:
            sub_cond = dict(main_condition)
            sub_cond.update({key1: v1})
            if key2 is not None:
                for v2 in values_2:
                    sub_cond_1 = dict(sub_cond)
                    sub_cond_1.update({key2: v2})
                    sub_conditions[(v1, v2)] = sub_cond_1
            else:
                sub_conditions[(v1,)] = sub_cond

        sub_conditions_files = {}
        items = [_ for _ in sub_conditions.items()]
        for key, cond in items:
            files = self.get_units_for_condition(cond)
            if len(files) == 0:
                del sub_conditions[key]
            else:
                sub_conditions_files[key] = files

        max_n_files_per_cond = max(
            [len(sub_conditions_files[cond]) for cond in sub_conditions_files]
        )
        # dimension of main plot
        n = len(values_1)
        m = max(len(values_2), 1)
        w = 3.0
        mult_f = 2 if show_files else 1
        mult = mult_f
        fig = plt.figure(figsize=(mult * m * w, n * w))
        outer = gridspec.GridSpec(n, mult * m, wspace=0.2, hspace=0.2)

        # dimension of subplots
        M = int(np.sqrt(max_n_files_per_cond))
        if M**2 < max_n_files_per_cond:
            L = M + 1
            if L * M < max_n_files_per_cond:
                M += 1
        else:
            L = M

        def plot_ax(ax, H: pd.DataFrame, title: str = ""):
            if not show_scatter:
                # Density
                xx, yy, w, w1, w2 = witness_function(
                    H.values[:, :2], H.values[:, :2], lims=self.lims
                )
                ax.contourf(xx, yy, w1, cmap="Reds", levels=20)
            else:
                ax.scatter(H.values[:, 0], H.values[:, 1], s=0.1)
                ax.set_xlim((self.lims[0], self.lims[1]))
                ax.set_ylim((self.lims[2], self.lims[3]))
            ax.set_title(title)
            ax.set_aspect("equal")
            plt.axis("off")

        for c_i, (key, cond) in enumerate(sub_conditions.items()):
            inner_main = gridspec.GridSpecFromSubplotSpec(
                1, 1, subplot_spec=outer[mult * c_i], wspace=0.1, hspace=0.1
            )
            n_ = 0
            m_ = 1
            for i, v in enumerate(values_1):
                if v == cond[key1]:
                    n_ = i
                    break
            for i, v in enumerate(values_2):
                if v == cond[key2]:
                    m_ = i
                    break
            # print(n_, m_, key)
            # ax = fig.add_subplot(n, m, 1 + (n_ * m + m_))
            ax = plt.Subplot(fig, inner_main[0])
            H = self.sample_latent_vecs_for_cond(
                cond, max_n_trajs_per_sample=max_trajs_per_plot
            )
            H, _ = keep_one_length_per_traj([H, H], cols_to_keep=self.latent_cols)
            H = H.sample(min(H.shape[0], max_trajs_per_plot))
            plot_ax(ax, H, title=" | ".join([str(v) for k, v in cond.items()]))
            fig.add_subplot(ax)
            if show_files:

                inner_files = gridspec.GridSpecFromSubplotSpec(
                    L, M, subplot_spec=outer[mult_f * c_i + 1], wspace=0.05, hspace=0.05
                )

                for i, f in enumerate(sub_conditions_files[key]):
                    ax = plt.Subplot(fig, inner_files[i])
                    H = self.latent_vecs[f]
                    H, _ = keep_one_length_per_traj([H, H], cols_to_keep=self.latent_cols)
                    H = H.sample(min(H.shape[0], max_trajs_per_plot))
                    plot_ax(ax, H)
                    fig.add_subplot(ax)
        plt.axis("off")
        plt.tight_layout()

    def plot_latent_map(
        self,
        min_length: int = 7,
        max_length: int = 15,
        fig_path: str = None,
        side: float = 1.0,
    ):
        """Plot a map of the latent space

        Args:
            min_length (int, optional): Minimal length of trajectories used for illustration. Defaults to 7.
            fig_path (str, optional): File path to save the plot. Defaults to None.
            side (float, optional): Side of the square vignettes used for calibration.
        """

        from sklearn.decomposition import PCA

        xx, yy, _, w, _ = witness_function(
            self.global_H.values,
            self.global_H.values,
            lims=self.lims,
            sigma=self.global_sigma,
            n_points=12,
        )
        n = xx.shape[0]
        m = xx.shape[1]
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111)
        outer = gridspec.GridSpec(n, m, wspace=0.1, hspace=0.1)
        L = side / 2
        d = max(xx[0, 1] - xx[0, 0], xx[1, 0] - xx[0, 0]) / 2
        plt.axis("off")
        for i in range(n):
            for j in range(m):
                point = np.array([xx[i, j], yy[i, j]])
                trajs = self.find_closest_trajectories(
                    latent_pos=point,
                    n_trajs=1,
                    max_dist=d,
                    min_length=min_length,
                    max_length=max_length,
                )
                if len(trajs) == 0:
                    continue
                else:
                    traj = trajs[0]
                    traj = traj - np.mean(traj, axis=0)
                    traj = PCA().fit_transform(traj)
                    traj = traj @ np.array([[1.0, 1.0], [-1.0, 1.0]]) / np.sqrt(2)
                    # traj -= traj[0]
                ax = plt.Subplot(fig, outer[(n - 1) - i, j])
                fig.add_subplot(ax)
                ax.plot(traj[:, 0], traj[:, 1])
                c_x = np.mean(traj[:, 0])
                c_y = np.mean(traj[:, 1])
                ax.set_xlim((c_x - L, c_x + L))
                ax.set_ylim((c_y - L, c_y + L))
                ax.set_xticks([])
                ax.set_yticks([])
                # plt.axis("off")
        # plt.axis("off")
        plt.tight_layout()
        if fig_path is not None:
            plt.savefig(fig_path)
        return

    def plot_mosaic(self):

        M = -np.log10(self._p_val)
        np.fill_diagonal(M, 0.0)

        w = 1.2
        fig = plt.figure(figsize=(w * M.shape[0], w * (M.shape[0] + 1)))
        ax = fig.add_subplot(111)

        dnorm = TwoSlopeNorm(
            vcenter=-np.log10(0.05), vmin=0.0, vmax=max(np.nanmax(M), 2)
        )
        cm = ax.matshow(M, norm=dnorm, cmap="coolwarm")
        cbar = plt.colorbar(cm, label="$P$", ticks=[0, 1, -np.log10(0.05), 2, 3])
        # cbar.ax.set_yticks()
        cbar.ax.set_yticklabels(["1", "0.1", "0.05", "0.01", "0.001"])

        plt.xticks(
            np.arange(len(self.groups)),
            [" | ".join([str(v) for _, v in g.items()]) for g in self.groups],
            rotation=90,
        )
        plt.yticks(
            np.arange(len(self.groups)),
            [" | ".join([str(v) for _, v in g.items()]) for g in self.groups],
        )
        plt.setp(ax.get_xticklabels(), rotation=-30, ha="right", rotation_mode="anchor")
        plt.tight_layout()

    def plot_all_comparisons(
        self,
        export_folder: str = None,
        max_abs: float = 0.01,
    ):
        if export_folder is not None:
            if not os.path.exists(export_folder):
                os.mkdir(export_folder)
            assert os.path.isdir(export_folder)

        # TODO: add self.lims
        cond1, cond2 = self.comparisons[0]
        H1 = self.sample_latent_vecs_for_cond(cond1, max_n_trajs_per_sample=1000)
        H2 = self.sample_latent_vecs_for_cond(cond2, max_n_trajs_per_sample=1000)
        H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)
        # sigma = get_sigma(H1, H2)
        H = pd.concat([H1, H2], axis=0)

        pgbar = tqdm(self.comparisons)
        for comp in pgbar:
            cond1, cond2 = comp
            desc = "_".join(
                [str(v) for v in list(cond1.values()) + list(cond2.values())]
            )
            pgbar.set_description_str(desc)

            # Latent densities
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_cond1_blues.pdf" % (desc))
            self.plot_density(cond1, cmap="Blues", fig_path=fig_path, close=True)
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_cond2_blues.pdf" % (desc))
            self.plot_density(cond2, cmap="Blues", fig_path=fig_path, close=True)
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_cond1_reds.pdf" % (desc))
            self.plot_density(cond1, cmap="Reds", fig_path=fig_path, close=True)
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_cond2_reds.pdf" % (desc))
            self.plot_density(cond2, cmap="Reds", fig_path=fig_path, close=True)

            # Witness function
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_witness.pdf" % (desc))
            self.plot_witness_function(
                cond1,
                cond2,
                lims=self.lims,
                fig_path=fig_path,
                close=True,
                max_abs=max_abs,
            )
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(
                    export_folder, "%s_witness_inverted.pdf" % (desc)
                )
            self.plot_witness_function(
                cond2,
                cond1,
                lims=self.lims,
                fig_path=fig_path,
                close=True,
                max_abs=max_abs,
            )

            # MMD histogram
            fig_path = None
            if export_folder is not None:
                fig_path = os.path.join(export_folder, "%s_MMD.pdf" % (desc))
            self.plot_MMD_histogram(
                cond1, cond2, fig_path=fig_path, close=True, hide_xticks=True
            )

    def plot_discriminant_trajs(
        self,
        cond1: Dict[str, Any],
        cond2: Dict[str, Any],
        show_critical_region: bool = False,
        n_bootstraps: int = 100,
        max_n_trajs: int = 20000,
        pair_critical_by: str = None,
        group_labels: dict = {},
        histogram_cols: dict = {},
        histogram_cols_to_log: List = [],
        fig_paths: dict = {},
    ):

        assert cond1 in self.groups
        assert cond2 in self.groups
        assert all([c in histogram_cols for c in histogram_cols_to_log])
        assert all([c in self.latent_df.columns for c in histogram_cols])

        assert not self.use_high_dimension, "Only available for 2D latent vectors"
        w1s, w2s = [], []
        l_cond1 = " | ".join(
            [
                str(v) if key not in group_labels else str(group_labels[key][v])
                for key, v in cond1.items()
            ]
        )
        l_cond2 = " | ".join(
            [
                str(v) if key not in group_labels else str(group_labels[key][v])
                for key, v in cond2.items()
            ]
        )

        # JUST FOR CALIBRATION OF LIMS
        H1 = self.sample_latent_vecs_for_cond(cond1, max_n_trajs_per_sample=max_n_trajs)
        H2 = self.sample_latent_vecs_for_cond(cond2, max_n_trajs_per_sample=max_n_trajs)
        # H1, H2 = keep_one_length_per_traj(H1, H2)

        # NOW BOOTSTRAP
        N = min(min(H1.traj_ID.nunique(), H2.traj_ID.nunique()) // 3, max_n_trajs)
        for i in tqdm(
            range(n_bootstraps),
            leave=False,
            desc="Bootstraping to get the statistics",
            disable=(n_bootstraps < 2000) | ~self.show_progress,
        ):
            H1 = self.sample_latent_vecs_for_cond(
                cond1, max_n_trajs_per_sample=max_n_trajs
            )
            H2 = self.sample_latent_vecs_for_cond(
                cond2, max_n_trajs_per_sample=max_n_trajs
            )

            H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)
            H1 = H1.sample(N)
            H2 = H2.sample(N)

            xx, yy, w, w1, w2 = witness_function(
                H1[self.latent_cols].values,
                H2[self.latent_cols].values,
                lims=self.lims,
                sigma=self.global_sigma,
            )
            w1s.append(w1)
            w2s.append(w2)

        # ESTIMATE STATISTICS
        w1 = np.stack(w1s, axis=0)
        w2 = np.stack(w2s, axis=0)
        w = w1 - w2
        mean_w = np.mean(w, axis=0)
        amplitude = mean_w**2
        var = np.var(w, axis=0)
        stat = N * amplitude / var
        stat[np.isnan(stat)] = 0.0
        w1 = np.mean(w1, axis=0)
        w2 = np.mean(w2, axis=0)

        # GET CRITICAL POINTS
        max_1 = maximum_position(stat, labels=mean_w > 0)
        max_2 = maximum_position(stat, labels=mean_w < 0)
        global_max = np.nanmax(stat)
        max_index, alt_max_index = max_1, max_2
        if np.max(stat[mean_w > 0]) < np.max(stat[mean_w < 0]):
            max_index, alt_max_index = alt_max_index, max_index
        # max_pos = np.array([xx[max_index], yy[max_index]])
        # alt_max_pos = np.array([xx[alt_max_index], yy[alt_max_index]])

        H1, units1 = self.sample_latent_vecs_for_cond(
            cond1, return_units=True, max_n_trajs_per_sample=max_n_trajs
        )
        H1["unit"] = units1
        H2, units2 = self.sample_latent_vecs_for_cond(
            cond2, return_units=True, max_n_trajs_per_sample=max_n_trajs
        )
        H2["unit"] = units2
        H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)

        # stat_signed > 0 autour de son max
        # stat_signed < 0 quand la deuxième cond prédomine
        stat_signed = stat * np.sign(mean_w * mean_w[max_index])
        assert np.sum(np.isnan(stat_signed)) == 0, "Stat is nan somewhere"
        stat_signed_estimator = scipy.interpolate.RectBivariateSpline(
            xx[0, :], yy[:, 0], stat_signed.T
        )
        stat_estimator = scipy.interpolate.RectBivariateSpline(
            xx[0, :], yy[:, 0], stat.T
        )

        stat_signed_at_points_1 = stat_signed_estimator.ev(H1["U_1"], H1["U_2"])
        stat_signed_at_points_2 = stat_signed_estimator.ev(H2["U_1"], H2["U_2"])
        stat_at_points_1 = stat_estimator.ev(H1["U_1"], H1["U_2"])
        stat_at_points_2 = stat_estimator.ev(H2["U_1"], H2["U_2"])

        try:
            assert np.sum(np.isnan(stat_at_points_1)) == 0, stat_at_points_1
        except:
            print(H1.head())
            print(H2.head())
            print(stat_signed.T)
            raise
        # close_to_max_point = np.linalg.norm(H1 - max_1_pos)

        stat_thresh_1 = stat_signed[max_1] * 0.75
        stat_thresh_2 = stat_signed[max_2] * 0.75
        stat_threshold = max(stat_thresh_1, stat_thresh_2)
        critical_mask = stat_signed > stat_threshold
        assert np.sum(critical_mask) > 0
        # critical_label = skimage.measure.label(critical_mask)
        # good_label = np.unique(critical_label[stat == global_max])[0]
        # critical_mask = critical_label == good_label

        self.logger.info("Interpolations are done")
        if stat_thresh_1 < 0:
            critic_1 = stat_signed_at_points_1 < stat_thresh_1
            critic_2 = stat_signed_at_points_2 > stat_thresh_2
        else:
            critic_1 = stat_signed_at_points_1 > stat_thresh_1
            critic_2 = stat_signed_at_points_2 < stat_thresh_2

        print("%d critic_1" % np.sum(critic_1))
        print("%d critic_2" % np.sum(critic_2))

        if show_critical_region:

            critic_unit_df_1 = pd.DataFrame.from_dict(
                {
                    "critic_spec": critic_1,  # critic of this condition's critic zone
                    "critic": stat_at_points_1 > stat_threshold,  # critic in general
                    "unit": H1.unit,
                    "stat": stat_at_points_1,
                    "stat_signed": stat_signed_at_points_1,
                    "U_1": H1["U_1"],
                    "U_2": H1["U_2"],
                }
            )
            critic_unit_df_1["critic_int"] = 1 * critic_unit_df_1["critic"]
            critic_unit_df_1["cond"] = l_cond1
            critic_unit_df_1.set_index(H1.index)

            critic_unit_df_2 = pd.DataFrame.from_dict(
                {
                    "critic_spec": critic_2,  # critic of this condition's critic zone
                    "critic": stat_at_points_2 > stat_threshold,  # critic in general
                    "unit": H2.unit,
                    "stat": stat_at_points_2,
                    "stat_signed": stat_signed_at_points_2,
                    "U_1": H2["U_1"],
                    "U_2": H2["U_2"],
                }
            )
            critic_unit_df_2["critic_int"] = 2 * critic_unit_df_2["critic"]
            critic_unit_df_2["cond"] = l_cond2
            critic_unit_df_2.set_index(H2.index)

            critic_unit_df = pd.concat(
                [critic_unit_df_1, critic_unit_df_2], axis=0, ignore_index=False
            )
            critic_unit_df.reset_index(inplace=True)
            # print(critic_unit_df.head())

            critic_unit_df = pd.merge(
                critic_unit_df,
                self.latent_df[
                    [c for c in self.latent_df.columns if c not in ["U_1", "U_2"]]
                ],
                left_on=["unit", "n", "L"],
                right_on=["unit", "n", "L"],
            )

            plt.figure()
            for critic, df in critic_unit_df.groupby("critic_int"):
                plt.scatter(
                    df["U_1"],
                    df["U_2"],
                    label="region %d" % critic,
                    s=0.005,
                )

            def rgb_to_hex(rgb):
                return "#%02x%02x%02x" % rgb

            blue = rgb_to_hex((35, 103, 246))
            red = rgb_to_hex((235, 75, 78))

            for col, col_label in histogram_cols.items():
                plt.figure(figsize=(3, 2))
                range_ = (critic_unit_df[col].min(), critic_unit_df[col].max())
                if col in histogram_cols_to_log:
                    range_ = (np.log10(range_[0]), np.log10(range_[1]))
                for (cond, df), color in zip(
                    critic_unit_df.groupby("cond"), [red, blue]
                ):
                    v = df[col]
                    v_critic = df.loc[df.critic_spec, col]
                    plt.hist(
                        v if not col in histogram_cols_to_log else np.log10(v),
                        # label=cond,
                        histtype="step",
                        color=color,
                        density=True,
                        range=range_,
                        bins=20,
                    )
                    plt.hist(
                        v_critic
                        if not col in histogram_cols_to_log
                        else np.log10(v_critic),
                        label=cond,
                        color=color,
                        alpha=0.5,
                        density=True,
                        range=range_,
                        bins=20,
                    )
                x_label = col_label
                if col in histogram_cols_to_log:
                    x_label += " ($\\log_{10}$)"
                plt.xlabel(x_label)
                plt.yticks([])
                plt.legend()
                plt.tight_layout()
                if "histograms" in fig_paths:
                    plt.savefig(os.path.join(fig_paths["histograms"], "%s.pdf" % col))

            plt.figure()
            # print(critic_unit_df.loc[critic_unit_df.cond == l_cond1].head())
            plt.scatter(
                critic_unit_df.loc[critic_unit_df.cond == l_cond1, "U_1"],
                critic_unit_df.loc[critic_unit_df.cond == l_cond1, "U_2"],
                c=critic_unit_df.loc[critic_unit_df.cond == l_cond1, "stat"],
                s=0.05,
                cmap="coolwarm",
                norm=TwoSlopeNorm(vcenter=0.0),
            )

            self.logger.info(critic_unit_df.unit.value_counts())

            # if pair_critical_by is not None:
            #    critic_unit_df = critic_unit_df.merge(
            #        self.latent_df.groupby("unit")[[pair_critical_by]].first(),
            #        left_on="unit",
            #        right_index=True,
            #        how="left",
            #    )

            critic_unit_count = critic_unit_df.loc[critic_unit_df.critic].pivot_table(
                values="critic",
                aggfunc="count",
                index=pair_critical_by if pair_critical_by is not None else "unit",
                columns="cond",
                fill_value=np.nan,
            )

            # PLOT THE ORIGIN OF TRAJECTORIES IN THE CRITICAL REGION
            fig2 = plt.figure(figsize=(7, 3))

            ax = fig2.add_subplot(111)
            # PROCEED ACCORDING TO WHETHER OBSERVATIONS ARE PAIRED OR NOT
            N_total = critic_unit_count.sum().sum()
            if pair_critical_by is None:
                n_units_1 = (~critic_unit_count[l_cond1].isnull()).sum()
                ax.bar(
                    np.arange(n_units_1) - 0.2,
                    critic_unit_count.loc[
                        ~critic_unit_count[l_cond1].isnull(), l_cond1
                    ].values
                    / N_total,
                    width=0.4,
                    color=blue,
                    label="%s"  # ($N=%d$)
                    % (l_cond1),  # critic_unit_count[l_cond1].sum(skipna=True)
                )
                n_units_2 = (~critic_unit_count[l_cond2].isnull()).sum()
                ax.bar(
                    np.arange(start=n_units_1, stop=n_units_2 + n_units_1) + 0.2,
                    critic_unit_count.loc[
                        ~critic_unit_count[l_cond2].isnull(), l_cond2
                    ].values
                    / N_total,
                    width=0.4,
                    color=red,
                    label="%s"  # ($N=%d$)
                    % (l_cond2),  # critic_unit_count[l_cond2].sum(skipna=True)
                )
            else:
                n_units = critic_unit_count.shape[0]
                ax.bar(
                    np.arange(n_units) - 0.2,
                    critic_unit_count[l_cond1].fillna(0).values / N_total,
                    width=0.4,
                    color=blue,
                    label="%s"  # ($N=%d$)
                    % (l_cond1),  # critic_unit_count[l_cond1].sum(skipna=True)
                )
                if critic_unit_count[l_cond1].isnull().sum() > 0:
                    ax.bar(
                        np.arange(n_units) - 0.2,
                        critic_unit_count[l_cond1].isnull()
                        * critic_unit_count.median(skipna=True).median()
                        / (N_total * 2),
                        width=0.4,
                        color=blue,
                        alpha=0.3,
                        label="no data for %s" % (l_cond1),
                    )
                ax.bar(
                    np.arange(n_units) + 0.2,
                    critic_unit_count[l_cond2].fillna(0).values / N_total,
                    width=0.4,
                    color=red,
                    label="%s"  # ($N=%d$)
                    % (l_cond2),  # critic_unit_count[l_cond2].sum(skipna=True)
                )
                if critic_unit_count[l_cond2].isnull().sum() > 0:
                    ax.bar(
                        np.arange(n_units) + 0.2,
                        critic_unit_count[l_cond2].isnull()
                        * critic_unit_count.median(skipna=True).median()
                        / (N_total * 2),
                        width=0.4,
                        color=red,
                        alpha=0.3,
                        label="no data for %s" % (l_cond2),
                    )
                ax.set_xticks(np.arange(n_units))
                ax.set_xticklabels(np.arange(n_units) + 1)
            # ax.set_title("Trajectories from the most salient region")
            yticks = mtick.PercentFormatter(1.0)
            ax.yaxis.set_major_formatter(yticks)
            ax.legend()
            ax.set_xlabel("Fields of view")
            ax.set_ylabel("Trajectories")
            plt.tight_layout()
            if "repartition" in fig_paths:
                plt.savefig(fig_paths["repartition"])
                self.logger.info(
                    "Saved repartition figure at %s" % fig_paths["repartition"]
                )

        fig = plt.figure(figsize=(8, 12.5))

        ax = fig.add_subplot(421)
        ax.scatter(
            H1["U_1"],
            H1["U_2"],
            c=stat_at_points_1,
            s=0.1,
            cmap="coolwarm",
            norm=TwoSlopeNorm(vcenter=0),
        )
        ax.set_xlim((np.min(xx), np.max(xx)))
        ax.set_ylim((np.min(yy), np.max(yy)))
        ax.set_aspect("equal")

        ax = fig.add_subplot(422)
        ax.scatter(
            H2["U_1"],
            H2["U_2"],
            c=stat_at_points_2,
            s=0.1,
            cmap="coolwarm",
            norm=TwoSlopeNorm(vcenter=0),
        )
        ax.set_xlim((np.min(xx), np.max(xx)))
        ax.set_ylim((np.min(yy), np.max(yy)))
        ax.set_aspect("equal")

        ax = fig.add_subplot(423)
        ax.contourf(xx, yy, w1, cmap="Blues", levels=20)
        ax.set_title(l_cond1)
        self._add_grid(ax, hide_ticks=True)

        ax = fig.add_subplot(424)
        ax.contourf(xx, yy, w2, cmap="Reds", levels=20)
        ax.set_title(l_cond2)
        self._add_grid(ax, hide_ticks=True)

        ax = fig.add_subplot(425)
        M = np.max(np.abs(mean_w))
        dnorm = TwoSlopeNorm(vcenter=0, vmin=-M, vmax=M)
        ax.set_title("Blue : %s\n Red : %s" % (l_cond1, l_cond2))
        ax.contourf(xx, yy, mean_w, levels=40, norm=dnorm, cmap="Spectral")
        # ax.contourf(xx, yy, stat_signed, levels=40, norm=dnorm, cmap="Spectral")
        self._add_grid(ax, hide_ticks=True)

        ax = fig.add_subplot(426)
        for i, g in enumerate(self.groups):
            if g == cond1:
                i1 = i
            if g == cond2:
                i2 = i
        ax.contourf(xx, yy, stat, cmap="Greens", levels=20)
        ax.set_title(
            "P-value of similarity : %.2e\nMost characteristic points"
            % self._p_val[i1, i2]
        )
        marker_1_props = dict(c="blue", marker="*")
        marker_2_props = dict(c="red", marker="^")
        ax.scatter(xx[max_1], yy[max_1], **marker_1_props, s=50)
        ax.scatter(xx[max_2], yy[max_2], **marker_2_props, s=50)
        if show_critical_region:
            ax.contour(xx, yy, critical_mask, levels=[0.5], antialiased=True)
        self._add_grid(ax, hide_ticks=True)

        self.logger.info("Looking at critical trajectories")

        # Trouver les trajectoires proches de chaque position
        point1 = np.array([xx[max_1], yy[max_1]])
        point2 = np.array([xx[max_2], yy[max_2]])

        n_trajs = 16
        trajs1 = self.find_closest_trajectories(
            latent_pos=point1,
            n_trajs=n_trajs,
            min_length=14,
            max_length=16,
            max_dist=0.5,
        )
        trajs2 = self.find_closest_trajectories(
            latent_pos=point2,
            n_trajs=n_trajs,
            min_length=14,
            max_length=16,
            max_dist=0.5,
        )

        L = 0.2
        M = int(np.ceil(np.sqrt(n_trajs)))

        for ax, point_props, trajs, label in zip(
            [fig.add_subplot(427), fig.add_subplot(428)],
            [marker_1_props, marker_2_props],
            [trajs1, trajs2],
            [l_cond1, l_cond2],
        ):
            for i, t in enumerate(trajs):
                t = np.array(t)
                t -= t[0]
                k = i // M
                l = i % M
                t[:, 0] += L * k
                t[:, 1] += L * l
                ax.plot(t[:, 0], t[:, 1])
            ax.scatter(L * (M - 1) / 2, (M + 0.6) * L, s=100, **point_props)
            ax.set_xlim((-2 * L, (M + 1) * L))
            ax.set_ylim((-2 * L, (M + 1) * L))
            ax.set_title("Characteristic trajectories of\n%s" % label)
            ax.set_axis_off()
            ax.plot(
                [-1.5 * L, -1.5 * L + 0.2],
                [-1.5 * L, -1.5 * L],
                lw=4,
                marker="|",
                c="black",
            )
            ax.text(s="200 nm", x=-1.5 * L + 0.1, y=-1.2 * L, ha="center")
        plt.tight_layout()
        if "comparison" in fig_paths:
            plt.savefig(fig_paths["comparison"])


class MMDInterGroupAnalysis(GroupBasedAnalysisStep):
    """
    Compare conditions between them and return p-values
    """

    def __init__(
        self,
        null_mode: str = "permutation",
        n_bootstraps_D_sigma: int = 20,  # number of bootstraps to estimate the variance of the MMD estimator
        **kwargs
    ):
        super(MMDInterGroupAnalysis, self).__init__(**kwargs)
        self.null_mode = null_mode
        self.n_bootstraps_D_sigma = n_bootstraps_D_sigma
        assert null_mode in ["permutation", "mix"]

    def params_dict(self):
        d = super().params_dict()
        d["n_bootstraps_D_sigma"] = self.n_bootstraps_D_sigma
        d["null_mode"] = self.null_mode
        return d

    def compare_conditions(self, cond1: dict, cond2: dict):

        D_true = 0.0

        units1 = self.get_units_for_condition(cond1)
        units2 = self.get_units_for_condition(cond2)

        d1 = " | ".join([str(v) for v in cond1.values()])
        d2 = " | ".join([str(v) for v in cond2.values()])

        # self.logger.info(
        #     "# units \n\t %s : %d\n\t %s : %d" % (d1, len(units1), d2, len(units2))
        # )

        if len(units1) == 0 or len(units2) == 0:
            self.logger.warning("Should not compare these conditions : %s and %s" % (cond1,cond2))
            return D_true, np.ones(1), {}
        # Get the minimal number of samples in all files
        min_n_1 = min(
            [
                self.latent_vecs[f].traj_ID.nunique()
                for f in self.get_units_for_condition(cond1)
            ]
        )
        min_n_2 = min(
            [
                self.latent_vecs[f].traj_ID.nunique()
                for f in self.get_units_for_condition(cond2)
            ]
        )
        min_n = min(
            min_n_1,
            min_n_2,
        )
        self.logger.debug("min_n = %d (%d, %d)" % (min_n, min_n_1, min_n_2))

        min_n = min(min_n, self.n_max_trajs_per_unit)
        # self.logger.debug("Min N = %d" % min_n)

        all_units = list(units1 + units2)
        sigma = self.global_sigma
        mmd_args = {"sigma": sigma, "unbiased": self.unbiased}

        D_true = np.full(self.n_bootstraps_D_sigma,np.nan)

        if self.null_mode == "permutation":
            n_combs = comb(len(all_units), len(units1))
            self.logger.debug("# possible combinations = %d" % n_combs)
            n_bootstraps_null = int(min(n_combs, self.n_max_bootstraps))
        elif self.null_mode == "mix":
            n_bootstraps_null = self.n_max_bootstraps
        
        # In case there are more bootstraps required for the true MMD than for the null hypothesis
        n_bootstraps = max(self.n_bootstraps_D_sigma, n_bootstraps_null)

        D_null = np.full(self.n_max_bootstraps,np.nan)

        infos = {
            "min_n_1": min_n_1,
            "min_n_2": min_n_2,
            "min_n": min_n,
            "n_units_1": len(units1),
            "n_units_2": len(units2),
            "n_bootstraps": n_bootstraps,
        }

        # We split files in filesA and filesB. filesA has the same length as files1
        desc = d1 + " VS " + d2

        if self.null_mode == "permutation" and n_bootstraps < self.n_max_bootstraps:
            # Generate a list of all subset of units of size len(units1)
            all_unitsA = [_ for _ in itertools.combinations(all_units, r=len(units1))]
            self.logger.debug("# generated combinations = %d" % len(all_unitsA))
            assert len(all_unitsA) >= n_bootstraps_null

        for i in tqdm(
            range(n_bootstraps),
            leave=False,
            total=n_bootstraps,
            desc=desc,
            disable=(n_bootstraps < 2000) | ~self.show_progress,
        ):
            
            #logging.warn("Bootstrap #%d (%d, %d)" % (i, self.n_bootstraps_D_sigma, self.n_max_bootstraps))

            if i < self.n_bootstraps_D_sigma:
                H1 = self._concat_equal_length_latent_dfs(units1, min_n)
                H2 = self._concat_equal_length_latent_dfs(units2, min_n)
                # equalize length distributions
                H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)
                H1, H2 = H1.values, H2.values
                D_true[i] = MMD2(H1, H2, **mmd_args)

            if i < n_bootstraps_null:

                if self.null_mode == "permutation":
                    if n_bootstraps == self.n_max_bootstraps:
                        units_ = np.random.permutation(all_units)
                        unitsA = units_[: len(units1)]
                        assert len(unitsA) == len(units1)
                        unitsB = units_[len(units1) :]
                    else:
                        unitsA = all_unitsA[i]
                        unitsB = list(set(all_units).difference(unitsA))
                    assert len(unitsB) == len(
                        units2
                    ), "filesB = %d vs files2 = %d | total = %d | files 1 = %d " % (
                        len(unitsB),
                        len(units2),
                        len(all_units),
                        len(units1),
                    )
                    HA = self._concat_equal_length_latent_dfs(unitsA, min_n)
                    HB = self._concat_equal_length_latent_dfs(unitsB, min_n)
                    HA, HB = keep_one_length_per_traj(
                        [HA, HB], cols_to_keep=self.latent_cols
                    )
                    HA, HB = HA.values, HB.values

                elif self.null_mode == "mix":
                    # We re-generate H1 and H2
                    H1 = self._concat_equal_length_latent_dfs(units1, min_n)
                    H2 = self._concat_equal_length_latent_dfs(units2, min_n)
                    # equalize length distributions
                    H1, H2 = keep_one_length_per_traj(
                        [H1, H2], cols_to_keep=self.latent_cols
                    )
                    H1, H2 = H1.values, H2.values

                    H = pd.concat(
                        [pd.DataFrame(H1), pd.DataFrame(H2)], axis=0, ignore_index=True
                    )
                    H = H.sample(H.shape[0], replace=False)
                    HA = H.iloc[: H1.shape[0]]
                    HB = H.iloc[H1.shape[0] :]
                else:
                    raise NotImplementedError(
                        "null_mode should be one of mix and permutation"
                    )

                assert HA.shape[0] == H1.shape[0]
                assert HB.shape[0] == H2.shape[0]
                D_null[i] = MMD2(HA, HB, **mmd_args)
            
            # Si on est sûr que la p-value est plus grande que 10 * 1/i, on stoppe ici (pas la peine de calculer avec précision les grandes p-values)
            if i >= self.n_bootstraps_D_sigma and i % 10 == 0: 
                D_true_est = np.mean(D_true)
                tirages = D_null[:i] > D_true_est
                k = np.sum(tirages)
                n = tirages.shape[0]
                p_target = 5./i
                p = binomtest(
                        k=k,
                        n=n,
                        p = p_target,
                        alternative = "less").pvalue
                if p > 0.99:
                    self.logger.debug("n_succes : %d\t n_tirages : %d\t p_lim : %s, p : %s" % (k, n, p_target, p))
                    self.logger.debug("Stopping at iteration %d / %d because the p-value will surely be larger than %.3f in the end" % (i, self.n_max_bootstraps, p_target))
                    missing_values = self.n_max_bootstraps - i
                    D_null[i:] = np.tile(D_null[:i], 1 + (missing_values // i))[:missing_values]
                    break 

        if n_bootstraps_null < self.n_max_bootstraps:
            n_missing_values = D_null[n_bootstraps_null:].shape[0]
            D_null[n_bootstraps_null:] = np.repeat(D_null[:n_bootstraps_null],int(self.n_max_bootstraps // n_bootstraps_null))[:n_missing_values]

        if n_bootstraps < self.n_bootstraps_D_sigma:
            n_missing_values = D_true[n_bootstraps:].shape[0]
            D_true[n_bootstraps:] = np.repeat(D_true[:n_bootstraps],int(self.n_bootstraps_D_sigma // n_bootstraps))[:n_missing_values]

        assert np.sum(np.isnan(D_true)) == 0
        assert np.sum(np.isnan(D_null)) == 0


        return D_true, D_null, infos

    def process_comparison(args):
        self, comp_hash = args
        assert comp_hash in self.equivalent_comparisons
        cond1, cond2 = self.equivalent_comparisons[comp_hash][0]
        self.logger.info("About to compare %s and %s" % (" / ".join([str(v) for v in cond1.values()])," / ".join([str(v) for v in cond2.values()])))
        D_true, D_null, infos = self.compare_conditions(cond1, cond2)
        return comp_hash, D_true, D_null, cond1, cond2, infos

    def _process(self):
        n = self.n_groups
        self._D_null = np.full((n, n, self.n_max_bootstraps),np.nan)
        self._D_true = np.full((n, n, self.n_bootstraps_D_sigma),np.nan)
        self._n_bootstraps = np.ones((n, n)) * self.n_max_bootstraps
        self._infos = {
            "min_n_1": np.zeros((n, n)),
            "min_n_2": np.zeros((n, n)),
            "min_n": np.zeros((n, n)),
            "n_units_1": np.zeros((n, n)),
            "n_units_2": np.zeros((n, n)),
        }

        args = [(self, c) for c in self.equivalent_comparisons]

        self.logger.debug("Prepared arguments for %d comparisons" % len(args))
        
        def process_returned(returned, tqdm_):
            comp_hash, D_true, D_null, cond1, cond2, infos = returned
            
            assert len(D_true) == self._D_true.shape[-1]
            
            assert comp_hash in self.equivalent_comparisons
            
            if len(self.equivalent_comparisons[comp_hash]) > 1:
                self.logger.debug("Equivalent comparisons : ")
                for c in self.equivalent_comparisons[comp_hash]:
                    self.logger.debug(c)
            
            for g1, g2 in self.equivalent_comparisons[comp_hash]:
                
                i1, i2 = self.groups.index(g1), self.groups.index(g2)
                assert i1 != i2 or self.compare_groups_with_self
            
                if len(infos) > 0:
                    self._D_null[i1, i2] = D_null
                    self._D_null[i2, i1] = D_null
                    self._D_true[i1, i2] = D_true
                    self._D_true[i2, i1] = D_true
                    self._infos["min_n"][i1, i2] = infos["min_n"]
                    self._infos["n_units_1"][i1, i2] = infos["n_units_1"]
                    self._infos["n_units_2"][i1, i2] = infos["n_units_2"]
                    self._infos["min_n"][i2, i1] = infos["min_n"]
                    self._infos["n_units_1"][i2, i1] = infos["n_units_2"]
                    self._infos["n_units_2"][i2, i1] = infos["n_units_1"]
                    self._n_bootstraps[i1, i2] = infos["n_bootstraps"]
                    self._n_bootstraps[i2, i1] = infos["n_bootstraps"]
                    tqdm_.desc = "(%d bootstraps) : %s VS %s" % (
                        infos["n_bootstraps"],
                        cond1,
                        cond2,
                    )
                else:
                    print("Could not compare %s and %s" % (cond1, cond2))

        # Pour les tests
        # mono_thread = True
        mono_thread = self.n_processes <= 1
        if mono_thread:
            self.logger.info("Doing things in a single thread")
            tqdm_ = tqdm(args, disable=(len(args) < 2000) | ~self.show_progress)
            for i, arg in enumerate(tqdm_):
                returned = MMDInterGroupAnalysis.process_comparison(arg)
                process_returned(returned, tqdm_)
                self.logger.info("Processed comparison %d / %d" % (i + 1, len(args)))

        else:
            self.logger.info("Using %d threads" % self.n_processes)
            p = Pool(processes=self.n_processes)
            tqdm_ = tqdm(
                p.imap_unordered(MMDInterGroupAnalysis.process_comparison, args),
                disable=(len(args) < 2000) | ~self.show_progress,
                total=int(len(args)),
            )
            for i, returned in enumerate(tqdm_):
                process_returned(returned, tqdm_)
                self.logger.info("Processed comparison %d / %d" % (i + 1, len(args)))

            p.close()
            p.join()

        self.logger.info("Finished comparisons")
        
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            mean_D_true = np.nanmean(self._D_true, axis=2)
        
            assert np.allclose(mean_D_true,mean_D_true.T,rtol=1e-03,atol=1e-05,equal_nan=True)
            self.logger.debug("mean_D_true symmetric")
            
            mean_D_null = np.nanmean(self._D_null,axis=2)
            assert np.allclose(mean_D_null,mean_D_null.T,rtol=1e-03,atol=1e-05,equal_nan=True)
            self.logger.debug("mean_D_null symmetric")
            
            self._p_val = np.clip(
                np.nanmean(self._D_null > np.expand_dims(mean_D_true, 2), axis=2),
                a_min=0.5 / self._n_bootstraps,
                a_max=2.0,
            )
        
        assert np.allclose(self._p_val,self._p_val.T,rtol=1e-03,atol=1e-05,equal_nan=True)
        
        self.logger.debug("initialized p-value")
        
        if not self.compare_groups_with_self:
            np.fill_diagonal(self._p_val, 1.0)
        self._p_val[np.sum(np.isnan(self._D_null), axis=2) > 0] = np.nan
        
        assert np.allclose(self._p_val,self._p_val.T,rtol=1e-03,atol=1e-05,equal_nan=True)
        
        self.logger.debug("Filled p-value matrix")

    def _save(self):
        super()._save()
        zarr.save(os.path.join(self.output_path, "p_val"), self._p_val)
        zarr.save(os.path.join(self.output_path, "D_null"), self._D_null)
        zarr.save(os.path.join(self.output_path, "D_true"), self._D_true)
        

    def _load_results(self):
        super()._load_results()
        self._p_val = zarr.load(os.path.join(self.output_path, "p_val"))
        self._D_null = zarr.load(os.path.join(self.output_path, "D_null"))
        self._D_true = zarr.load(os.path.join(self.output_path, "D_true"))

    @classmethod
    def name_prefix(cls) -> str:
        return "MMD_inter_groups"

    @property
    def output_names(self) -> Union[str, List[str]]:
        return ["D_null", "D_true", "p_val", "groups.json","comparisons.json", "latent_df.csv"]


class MMDInterGroupPairedAnalysis(GroupBasedAnalysisStep):
    """
    Pairwise comparisons of groups, obtained by botstraping units.
    The p-value computed is that of the Pearson correlation between the MMD and the difference of composition of random splits (in terms of units from group 1 vs units from group 2).
    """

    def __init__(self, pair_by: List, **kwargs):
        self.pair_by = pair_by
        logging.warn("This class is under development, use with caution")
        super(MMDInterGroupPairedAnalysis, self).__init__(**kwargs)

    def get_pairs_with_both_items(self, units1: List, units2: List):
        df = self.latent_df
        pairs = {}
        for value, df_ in df.groupby(self.pair_by):
            df_units = set(df_.unit.unique().tolist())
            if len(df_units) != 2:
                continue
            if len(df_units.intersection(units1)) != 1:
                continue
            if len(df_units.intersection(units2)) != 1:
                continue
            unit1 = list(df_units.intersection(units1))[0]
            unit2 = list(df_units.intersection(units2))[0]
            pairs[value] = (unit1, unit2)
        self.logger.info("Found %d valid pairs" % len(pairs))
        return pairs

    def process_comparison(args):
        self, cond_pair = args
        cond1, cond2 = cond_pair
        i1, i2 = -1, -1
        for i, cond in enumerate(self.groups):
            if cond == cond1:
                i1 = i
            elif cond == cond2:
                i2 = i
        assert min(i1, i2) > -1, "i1 and i2 were not retrieved"
        D_true, D_null, diff_null, infos = self.compare_conditions(cond1, cond2)
        return i1, i2, D_true, D_null, diff_null, cond1, cond2, infos

    def compare_conditions(self, cond1: dict, cond2: dict):

        units1 = self.get_units_for_condition(cond1)
        units2 = self.get_units_for_condition(cond2)
        assert len(set(units1).intersection(units2)) == 0, "Cannot have units in common"

        self.logger.info("n units = %d, %d" % (len(units1), len(units2)))

        pairs = self.get_pairs_with_both_items(units1, units2)
        units1 = [pair[0] for i, pair in pairs.items()]
        units2 = [pair[1] for i, pair in pairs.items()]
        assert len(units1) == len(units2)

        n_combs = 2 ** len(pairs)
        self.logger.info("n combinations = %d" % n_combs)
        n_bootstraps = int(min(n_combs, self.n_max_bootstraps))
        D_null = np.empty(self.n_max_bootstraps)
        diff_null = np.empty(self.n_max_bootstraps)

        if len(units1) == 0 or len(units2) == 0:
            return 0.0, D_null, diff_null, {}
        # Get the minimal number of samples in all files
        min_n_1 = min([self.latent_vecs[f].traj_ID.nunique() for f in units1])
        min_n_2 = min([self.latent_vecs[f].traj_ID.nunique() for f in units2])
        min_n = min(
            min_n_1,
            min_n_2,
        )
        self.logger.debug("min_n = %d (%d, %d)" % (min_n, min_n_1, min_n_2))

        min_n = min(min_n, self.n_max_trajs_per_unit)
        # self.logger.debug("Min N = %d" % min_n)
        H1 = self._concat_equal_length_latent_dfs(units1, min_n).values
        H2 = self._concat_equal_length_latent_dfs(units2, min_n).values
        H1, H2 = keep_one_length_per_traj([H1, H2], cols_to_keep=self.latent_cols)
        all_units = list(units1 + units2)
        sigma = self.global_sigma
        mmd_args = {"sigma": sigma, "unbiased": self.unbiased}
        D_true = MMD2(H1, H2, **mmd_args)

        infos = {
            "min_n_1": min_n_1,
            "min_n_2": min_n_2,
            "min_n": min_n,
            "n_units_1": len(units1),
            "n_units_2": len(units2),
            "n_bootstraps": n_bootstraps,
        }

        # We split files in filesA and filesB. filesA has the same length as files1
        desc = " | ".join(cond1.values()) + " VS " + " | ".join(cond2.values())
        for i in tqdm(
            range(n_bootstraps),
            leave=False,
            total=n_bootstraps,
            desc=desc,
            disable=(n_bootstraps < 2000) | ~self.show_progress,
        ):
            if n_bootstraps == self.n_max_bootstraps:
                units_1_in_a = np.random.choice([0, 1], size=len(units1), replace=True)
            else:
                bin_str = str(bin(i))[2:].rjust(len(units1), "0")
                units_1_in_a = [int(s) for s in list(bin_str)]
            unitsA = []
            unitsB = []
            for j, pair in zip(units_1_in_a, pairs.values()):
                unitsA.append(pair[int(j)])
                unitsB.append(pair[1 - int(j)])
            assert len(unitsB) == len(
                units2
            ), "filesB = %d vs files2 = %d | total = %d | files 1 = %d " % (
                len(unitsB),
                len(units2),
                len(all_units),
                len(units1),
            )
            HA = self._concat_equal_length_latent_dfs(unitsA, min_n).values
            HB = self._concat_equal_length_latent_dfs(unitsB, min_n).values
            assert HA.shape[0] == H1.shape[0]
            assert HB.shape[0] == H2.shape[0]
            D_null[i] = MMD2(HA, HB, **mmd_args)
            diff_null[i] = abs(len(units1) - 2 * sum(units_1_in_a))

        return D_true, D_null, diff_null, infos

    def _process(self):
        n = self.n_groups
        self._D_null = np.zeros((n, n, self.n_max_bootstraps))
        self._diff_null = np.zeros((n, n, self.n_max_bootstraps))
        self._D_true = np.zeros((n, n, self.n_bootstraps_D_sigma))
        self._n_bootstraps = np.ones((n, n)) * self.n_max_bootstraps

        self._infos = {
            "min_n_1": np.zeros((n, n)),
            "min_n_2": np.zeros((n, n)),
            "min_n": np.zeros((n, n)),
            "n_units_1": np.zeros((n, n)),
            "n_units_2": np.zeros((n, n)),
        }

        args = [(self, c) for c in self.comparisons]

        def process_returned(returned, tqdm_):
            i1, i2, D_true, D_null, diff_null, cond1, cond2, infos = returned
            self._D_null[i1, i2] = D_null
            self._D_null[i2, i1] = D_null
            self._diff_null[i1, i2] = diff_null
            self._diff_null[i2, i1] = diff_null
            self._D_true[i1, i2] = D_true
            self._D_true[i2, i1] = D_true
            self._infos["min_n"][i1, i2] = infos["min_n"]
            self._infos["n_units_1"][i1, i2] = infos["n_units_1"]
            self._infos["n_units_2"][i1, i2] = infos["n_units_2"]
            self._n_bootstraps[i1, i2] = infos["n_bootstraps"]
            self._n_bootstraps[i2, i1] = infos["n_bootstraps"]
            tqdm_.desc = "(%d bootstraps) : %s VS %s" % (
                infos["n_bootstraps"],
                cond1,
                cond2,
            )

        # Pour les tests
        test = False
        mono_process = test or (self.n_processes <= 1)
        if mono_process:
            tqdm_ = tqdm(args)
            for arg in tqdm_:
                returned = MMDInterGroupPairedAnalysis.process_comparison(arg)
                process_returned(returned, tqdm_)

        else:
            p = Pool(processes=self.n_processes)
            tqdm_ = tqdm(
                p.imap_unordered(MMDInterGroupPairedAnalysis.process_comparison, args),
                total=int(len(args)),
            )
            for returned in tqdm_:
                process_returned(returned, tqdm_)

            p.close()
            p.join()

        self._p_val = np.empty_like(self._D_true)
        for i in range(self._p_val.shape[0]):
            for j in range(self._p_val.shape[0]):
                try:
                    self._p_val[i, j] = pearsonr(
                        self._D_null[i, j], self._diff_null[i, j]
                    )[1]
                except:
                    self._p_val[i, j] = 1.0

        np.fill_diagonal(self._p_val, 1.0)
        self._p_val[np.nanstd(self._D_null, axis=2) == 0] = np.nan

    def plot_diff_MMD(self):
        for i in range(0, self._p_val.shape[0]):
            for j in range(i + 1, self._p_val.shape[1]):
                fig = plt.figure()
                ax = fig.add_subplot(111)
                valid_indices = (
                    np.arange(self._diff_null.shape[2]) < self._n_bootstraps[i, j]
                )
                plt.scatter(
                    np.reshape(self._diff_null[i, j][valid_indices], (-1,)),
                    np.reshape(self._D_null[i, j][valid_indices], (-1,)),
                    s=5,
                )
                g1 = "/".join(self.groups[i].values())
                g2 = "/".join(self.groups[j].values())
                plt.xlabel("$|N_{%s} - N_{%s}|$" % (g1, g2))
                plt.ylabel("$MMD^2$")
                plt.title(g1 + " VS " + g2)

    def _save(self):
        super()._save()
        zarr.save(os.path.join(self.output_path, "p_val"), self._p_val)
        zarr.save(os.path.join(self.output_path, "D_null"), self._D_null)
        zarr.save(os.path.join(self.output_path, "D_true"), self._D_true)

    def _load_results(self):
        super()._load_results()
        self._p_val = zarr.load(os.path.join(self.output_path, "p_val"))
        self._D_null = zarr.load(os.path.join(self.output_path, "D_null"))
        self._D_true = zarr.load(os.path.join(self.output_path, "D_true"))

    @classmethod
    def name_prefix(cls) -> str:
        return "MMD_groups_paired"

    @property
    def output_names(self) -> Union[str, List[str]]:
        return [
            "paired_D_null",
            "paired_D_true",
            "paired_p_val",
        ]  # , "condition_files.json"]


class MMDInterUnitAnalysis(MMDInterGroupAnalysis):
    """
    This class is meant to compute the MMD between pairs of units,
    using bootstraps to estimate the uncertainty in the measurement.
    It does NOT provide p-values of the coùparisons.
    To get p-values, use comparisons by groups.
    """

    def __init__(self, **kwargs):
        forced = ["null_mode", "n_max_bootstraps","unit_key","hierarchy","min_granularity"]
        for arg in forced:
            if arg in kwargs:
                del kwargs[arg]
        assert "group_by_keys" in kwargs, "Must specify group_by_keys"
        hierarchy = {}
        for key in kwargs["group_by_keys"]:
            hierarchy[key] = 0

        super(MMDInterUnitAnalysis, self).__init__(
            unit_key=["file"], 
            null_mode="mix", 
            hierarchy=hierarchy,
            min_granularity=len(hierarchy),
            n_max_bootstraps=0, **kwargs
        )

    @classmethod
    def name_prefix(cls) -> str:
        return "MMD_inter_units"

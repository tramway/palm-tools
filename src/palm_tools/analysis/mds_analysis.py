from __future__ import annotations
import os
import logging
from typing import TYPE_CHECKING, Callable, List, Union
from palm_tools.analysis.analysis_classes import AnalysisStep
from palm_tools.analysis.analysis_tools.mds_prob import MDS_prob, MDS_prob_bootstrap
from .mmd_analysis import MMDInterUnitAnalysis
import numpy as np
import pandas as pd



if TYPE_CHECKING:
    from palm_tools.data_structure.trackset import TrackSets


class MDSAnalysis(AnalysisStep):
    def __init__(
        self,
        track_sets: TrackSets,
        run_name: str,
        mmd_name: str = "example",  # In the future, should be just a JSON path
        filter_func: Callable = None,
        dimension: int = 2,
        logger: logging.Logger = None
    ):
        self.mmd_name = mmd_name
        self.mmd = MMDInterUnitAnalysis.from_params(
            export_folder=track_sets.root_folder, run_name=mmd_name, tss=track_sets
        )
        assert self.mmd.is_already_processed
        self.filter_func = filter_func
        self.use_bootstrap = False
        self.dim = dimension

        super(MDSAnalysis, self).__init__(tracksets=track_sets, run_name=run_name, logger=logger)

    def params_dict(self) -> dict:
        d = super().params_dict()
        d["dimension"] = self.dim
        d["mmd_name"] = self.mmd_name
        return d

    @classmethod
    def name_prefix(cls) -> str:
        return "MDS"

    @property
    def output_names(self) -> Union[str, List[str]]:
        return ["unit_info.csv"]

    def _save(self):
        self._unit_info.to_csv(
            os.path.join(self.output_path, "unit_info.csv"), index=True
        )

    def _load_results(self):
        self._unit_info = pd.read_csv(
            os.path.join(self.output_path, "unit_info.csv"), index_col=0
        )

    @property
    def unit_info(self) -> pd.DataFrame:
        if not hasattr(self, "_unit_info"):
            cols = [
                c
                for c in self.mmd.latent_df.columns
                if (
                    c not in self.mmd.latent_cols
                    and c not in self.tracksets.index_df.columns
                )
                or c == "file"
            ]
            self._unit_info = self.mmd.latent_df.groupby(self.mmd.group_by_keys)[cols].first().reset_index(drop=True)
            self._unit_info = self._unit_info.merge(
                self.tracksets.index_df,
                left_on="file",
                right_on="file",
                how="left",
            )
        return self._unit_info

    def _process(self):
        import torch
        indices = [
            i
            for i in range(len(self.mmd.latent_vecs))
            if i in self.unit_info.index.tolist()
        ]
        assert len(indices) == self.unit_info.shape[0], "%d vs %d" % (
            len(indices),
            self.unit_info.shape[0],
        )

        if self.use_bootstrap:
            D = self.mmd._D_true[np.ix_(indices, indices)]
            np.fill_diagonal(D,val=0.)
            assert np.sum(np.isnan(D)) == 0, "There are null values in D :%s" % D
            self._mds = MDS_prob_bootstrap(target_dim=self.dim)
            X = self._mds.fit_transform(D, n_steps=1000)
        else:
            D = np.mean(self.mmd._D_true[np.ix_(indices, indices)], axis=2)
            np.fill_diagonal(D,val=0)
            assert np.sum(np.isnan(D)) == 0, "There are null values in D :%s" % D
            D_sigma = np.std(self.mmd._D_true[np.ix_(indices, indices)], axis=2)
            np.fill_diagonal(D_sigma,val=1)
            assert np.sum(np.isnan(D_sigma)) == 0, "There are null values in D_sigma :%s" % D
            self._mds = MDS_prob(target_dim=self.dim)
            X = self._mds.fit_transform(
                D,
                D_sigma,
                device="cuda:0" if torch.cuda.is_available() else "cpu",
                n_steps=1000,
            )
        self.unit_info[["X_%d" % d for d in range(1, self.dim + 1)]] = X

    def plot_scatter(self, groupby: str = None):
        import matplotlib.pyplot as plt
        from scipy.stats import gaussian_kde

        dim = self.dim

        fig = plt.figure(figsize=(6, 6))
        for i in range(1, dim + 1):
            for j in range(1, dim + 1):
                n = (i - 1) * dim + j
                ax = fig.add_subplot(dim, dim, n)
                x_lim = (
                    self.unit_info["X_%d" % j].min() - 0.05,
                    self.unit_info["X_%d" % j].max() + 0.05,
                )
                y_lim = (
                    self.unit_info["X_%d" % i].min() - 0.05,
                    self.unit_info["X_%d" % i].max() + 0.05,
                )
                for cond, s in (
                    self.unit_info.groupby(groupby)
                    if groupby is not None
                    else [("all", self.unit_info)]
                ):
                    if i == j:
                        h, bins = np.histogram(
                            s["X_%d" % i], density=True, range=x_lim, bins=100
                        )
                        bins = 0.5 * (bins[1:] + bins[:-1])
                        h = gaussian_kde(s["X_%d" % i]).evaluate(bins)
                        ax.plot(bins, h, label=cond)
                    else:
                        ax.scatter(s["X_%d" % j], s["X_%d" % i], label=cond, s=5)
                        ax.set_xlim(x_lim)
                        ax.set_ylim(y_lim)
                ax.set_xticks([])
                ax.set_yticks([])
                if i == 1:
                    ax.set_title("MDS %d" % j, fontsize=12)
                if j == 1:
                    ax.set_ylabel("MDS %d" % i, fontsize=12)
        plt.tight_layout()

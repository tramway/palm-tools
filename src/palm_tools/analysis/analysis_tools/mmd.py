import logging
import numpy as np
from scipy.spatial import distance_matrix
from sklearn.neighbors import radius_neighbors_graph, NearestNeighbors
from sklearn.metrics import pairwise_distances
import pandas as pd
from typing import List, Union


def get_target_lengths(L_max, L_all):
    """Equalizes the length distributions between arrays of trajectory lengths

    Args:
        L1 (_type_): _description_
        L2 (_type_): _description_

    Returns:
        _type_: _description_
    """
    
    indices = [l_max.index.tolist() for l_max in L_max]
    sorted_L = [np.sort(l_max) for l_max in L_max]
    M = max([l_max.shape[0] for l_max in L_max])

    for i in range(M):
        index = [int(float(i / M) * l_max.shape[0]) for l_max in L_max]
        m = min([sorted_L[index] for sorted_L, index in  zip(sorted_L,index)])
        for j in range(len(sorted_L)):
            sorted_L[j][index[j]] = m
            
    adjusted_L = [sorted[np.argsort(np.argsort(l_max))] for sorted, l_max in zip(sorted_L,L_max)]

    for j, l_all in enumerate(L_all):
        for i in range(len(adjusted_L[j])):
            available_lengths = np.array(l_all[indices[j][i]])
            dist_to_target = np.abs(available_lengths - adjusted_L[j][i])
            adjusted_L[j][i] = available_lengths[np.argmin(dist_to_target)]

    # logging.warn("Mean distance to target %.2f" % np.mean(min_dist))

    return adjusted_L


def keep_one_length_per_traj(
    hs = List[pd.DataFrame],
    traj_ID_col: str = "traj_ID",
    cols_to_keep: List = None,
) -> pd.DataFrame:
    """return extracts of dataframes in hs with the same distirbution of trajectory length.
    traj_ID_col and 'L' must be columns of hs dataframes

    Args:
        hs (List[pd.DataFrame]): dataframes to filter
        traj_ID_col (str): identifier for trajectories

    Returns:
        _type_: _description_
    """
    
    gs = [h.groupby(traj_ID_col) for h in hs]
    L_max = [g.agg({"L": "max"})["L"] for g in gs]
    L = [g.agg({"L": lambda x: x.unique().tolist()})["L"].to_dict() for g in gs]

    L_f = get_target_lengths(L_max, L)

    sels = []
    for i, g in enumerate(gs):
        sel = pd.DataFrame(index=np.arange(L_max[i].shape[0]))
        sel[traj_ID_col] = L_max[i].index
        sel["L"] = L_f[i]
        sel.set_index([traj_ID_col, "L"], inplace=True)
        sels.append(sel)

    if cols_to_keep is None:
        return tuple([h.set_index([traj_ID_col, "L"]).loc[sel.index] for h, sel in zip(hs,sels)])
    else:
        return tuple([h.set_index([traj_ID_col, "L"]).loc[sel.index, cols_to_keep] for h, sel in zip(hs,sels)])

def h(Z, sigma):
    D = Z.shape[1] // 2
    X = Z[:, :D]
    Y = Z[:, D:]
    dxx = distance_matrix(X, X)
    dyy = distance_matrix(Y, Y)
    dxy = distance_matrix(X, Y)
    kxx = k_(dxx, sigma)
    kyy = k_(dyy, sigma)
    kxy = k_(dxy, sigma)
    kyx = kxy.T
    H = kxx + kyy - (kxy + kyx)
    return H


def get_sigma_2_u(X, Y, sigma):
    # sigma_u_2 = 4*(E_z[(E_{z'} h(z,z'))**2] - E_{z,z'}[h(z,z')]**2)
    N = X.shape[0]
    Z = np.concatenate([X, Y], axis=1)
    H = h(Z, sigma)
    np.fill_diagonal(H, np.nan)
    first_term = np.nanmean(np.nanmean(H, axis=1) ** 2)
    second_term = np.nanmean(H) ** 2
    return 4 * (first_term - second_term)


def k_(d_, sigma):
    d = np.copy(d_)
    d[d > 3 * sigma] = np.inf
    return np.exp(-(d**2) / (2 * (sigma**2))) / (np.sqrt(2 * np.pi) * sigma)


def k_unif(d_, sigma):
    d = np.copy(d_)
    return (1.0 / sigma) * (d < sigma)


def k_epanechnikov(d_, sigma):
    d = np.copy(d_)
    d[d > sigma] = sigma
    d /= sigma
    return 0.75 * (1 - d**2) * (1 / sigma**2)


def MMD2(
    X, Y, sigma, unbiased=False, A_X=None, A_Y=None, A_XY=None, kernel_type="gaussian"
):
    m = X.shape[0]
    n = Y.shape[0]

    if A_X is None:
        A_X = radius_neighbors_graph(
            X,
            radius=3 * sigma,
            mode="distance",
            include_self=not unbiased,
            n_jobs=1,
        ).data
    if A_Y is None:
        A_Y = radius_neighbors_graph(
            Y,
            radius=3 * sigma,
            mode="distance",
            include_self=not unbiased,
            n_jobs=1,
        ).data
    if A_XY is None:
        nn = NearestNeighbors(radius=3 * sigma, n_jobs=1)
        nn.fit(X)
        A_XY = nn.radius_neighbors_graph(Y, mode="distance").data

    if kernel_type == "gaussian":
        f = k_
    elif kernel_type == "uniform":
        f = k_unif
    elif kernel_type == "epanechnikov":
        f = k_epanechnikov
    else:
        raise "Unknown kernel type %s" % kernel_type

    mmd_2 = (
        (1.0 / (m * (m - 1 * unbiased))) * np.sum(f(A_X, sigma))
        + (1.0 / (n * (n - 1 * unbiased))) * np.sum(f(A_Y, sigma))
        - (2.0 / (m * n)) * np.sum(f(A_XY, sigma))
    )

    return mmd_2


def get_sigma(X, Y):
    D = pairwise_distances(X, Y)
    return (1.0 / 3) * np.median(D[D > 0])

def get_latent_grid(lims: tuple=None, n_points: int = 50, X:np.ndarray = None, Y: np.ndarray = None):
    # Define grid
    if lims is None:
        assert X is not None
        assert Y is not None
        x_min, x_max = min(np.min(X[:, 0]), np.min(Y[:, 0])), max(
            np.max(X[:, 0]), np.max(Y[:, 0])
        )
        y_min, y_max = min(np.min(X[:, 1]), np.min(Y[:, 1])), max(
            np.max(X[:, 1]), np.max(Y[:, 1])
        )
    else:
        x_min, x_max, y_min, y_max = lims

    x_values = np.linspace(x_min, x_max, num=n_points, endpoint=True)
    y_values = np.linspace(y_min, y_max, num=n_points, endpoint=True)
    xx, yy = np.meshgrid(x_values, y_values)
    return xx, yy

def witness_function(X, Y, lims: tuple = None, sigma: float = None, n_points: int = 50):

    if sigma is None:
        sigma = get_sigma(X, Y)
    xx, yy = get_latent_grid(lims, n_points, X, Y)

    def kernel_1(v):
        d = np.linalg.norm(X - np.reshape(v, (1, 2)), axis=1)
        return np.mean(k_(d, sigma))

    def kernel_2(x):
        d = np.linalg.norm(Y - np.reshape(v, (1, 2)), axis=1)
        return np.mean(k_(d, sigma))

    w1, w2 = np.zeros(xx.shape), np.zeros(xx.shape)
    for i in range(xx.shape[0]):
        for j in range(xx.shape[1]):
            v = np.array([xx[i, j], yy[i, j]])
            w1[i, j] = kernel_1(v)
            w2[i, j] = kernel_2(v)
    w = w1 - w2

    return xx, yy, w, w1, w2

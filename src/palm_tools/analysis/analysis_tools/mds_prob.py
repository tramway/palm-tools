from torch.nn import Parameter
import torch
import numpy as np
import matplotlib.pyplot as plt
from tqdm.contrib.logging import logging_redirect_tqdm
import logging


def upper_half(M):
    """
    Returns coefficients located above the diagonal, under the form of a 1D array
    """
    t = torch.tril(torch.ones_like(M), diagonal=-1)
    mask = t > 0
    assert torch.sum(mask) == M.shape[0] * (M.shape[1] - 1) / 2
    return M[mask]
    # indices = torch.tril_indices(M.shape[0], M.shape[1], offset=-1)


class MDS_prob_bootstrap:
    def __init__(self, target_dim=2):
        self.d = target_dim

    def fit_transform(self, D, lr=0.01, n_steps=int(1e4), move_frac=0.9):
        """
        D: matrix of squared MMD
        D_sigma: estimation of the standard deviation of the squared MMD
        move_frac: fraction of points moved at each iteration

        """
        assert move_frac > 0.0
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        N = D.shape[0]
        X = Parameter(
            torch.randn((N, self.d), device=device) * np.sqrt(np.median(D[D > 0]))
        )
        opt = torch.optim.Adam(lr=lr, params=[X])

        # Make Torch tensors
        D_ = torch.from_numpy(D).float().to(device)

        indices = torch.tril_indices(D.shape[0], D.shape[1], offset=-1)
        self.Ps = []  # One Probability density per coefficient
        s = torch.std(D_, dim=2) / 10
        for k in range(indices.shape[1]):
            i = indices[0, k]
            j = indices[1, k]

            mix = torch.distributions.Categorical(
                torch.ones(D.shape[2], device=D_.device)
            )
            comp = torch.distributions.Normal(
                D_[i, j], torch.ones_like(D_[i, j]) * s[i, j]
            )
            D_ij = torch.distributions.MixtureSameFamily(mix, comp)
            self.Ps.append(D_ij)

        assert len(self.Ps) == D.shape[0] * (D.shape[1] - 1) / 2

        def loss(x):
            d_reduced = upper_half(torch.cdist(x, x) ** 2)
            result = 0.0
            for elt, P in zip(d_reduced, self.Ps):
                result -= P.log_prob(elt)
            result /= len(self.Ps)
            return result

        try:
            for i in range(n_steps):
                opt.zero_grad()
                X_old = X.clone()
                l = loss(X)
                l.backward()
                opt.step()
                if i % 100 == 0:
                    print(l.item())
                if i % 100 == 0 and i < 0.75 * n_steps:
                    # Every 100 steps, except towards the end of the optimization
                    # Random perturbation
                    X.data = X.data + 0.2 * torch.randn_like(X.data) * torch.std(X.data)
                random_rows = torch.rand(N) < (1.0 - move_frac)
                X[random_rows].data = X_old[random_rows].data
        except KeyboardInterrupt:
            pass

        return X.detach().cpu().numpy()


from torch.nn import Parameter
import torch
import numpy as np
from sklearn.manifold import MDS
from tqdm import trange

"""
Author : Hippolyte Verdier, 2021
Modified by Alexandre Blanc, 2022
"""


class MDS_prob:
    def __init__(self, target_dim=2):
        self.d = target_dim

    def fit_transform(self, D, D_sigma2, lr=0.01, n_steps=int(1e4), device="cuda"):
        """
        D: matrix of squared MMD
        D_sigma2: estimation of the variance of the squared MMD

        """

        N = D.shape[0]

        # Initialize with deterministic MDS
        rect_D = np.sqrt(np.maximum(D, np.zeros_like(D)))
        init = MDS(n_components=self.d, dissimilarity="precomputed", normalized_stress="auto").fit_transform(
            rect_D
        )
        init = init.reshape(1, N, self.d)  # reshape to match hypotheses of cdist

        # Set up parameters and optimizer
        X = Parameter(torch.from_numpy(init).to(device))
        opt = torch.optim.Adam(lr=lr, params=[X])

        # Set up inputs as tensors
        D_ = torch.from_numpy(D).float().to(device)
        D_ = torch.tril(D_)
        D_ = D_.reshape(1, N, N)

        D_sigma2_ = torch.from_numpy(D_sigma2).float().to(device)
        D_sigma2_[D_sigma2_ == 0] = 0.5 * torch.min(D_sigma2_[D_sigma2_ > 0])
        D_sigma2_ = D_sigma2_.reshape(1, N, N)

        # Training loop
        loss_history = []
        refresh_every_steps = 50
        logging.basicConfig(level=logging.DEBUG)
        with logging_redirect_tqdm():
            with trange(n_steps) as steps_bar:
                for i in steps_bar:
                    opt.zero_grad()
                    l = self.loss(X, D_, D_sigma2_)
                    l.backward()
                    opt.step()
                    loss_history.append(l.item())
                    if i % refresh_every_steps == 0:
                        steps_bar.set_postfix(
                            loss=np.mean(loss_history[-refresh_every_steps:])
                        )

        self.loss_history = loss_history
        self.loss = loss_history[-1]
        self.X = X.detach().cpu().numpy().squeeze()

        return self.X

    def fit(self, *args, **kwargs):
        self.fit_transform(*args, **kwargs)

    def loss(self, X, D, D_sigma2):
        d_reduced = torch.cdist(X, X) ** 2
        pairwise_loglikelihood = -0.5 * (d_reduced - D) ** 2 / D_sigma2
        loglikelihood = torch.mean(
            torch.tril(pairwise_loglikelihood)
        )  # Note the usage of tril
        return -loglikelihood

"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
``[options.entry_points]`` section in ``setup.cfg``::

    console_scripts =
         fibonacci = palm_tools.skeleton:run

Then run ``pip install .`` (or ``pip install -e .`` for editable mode)
which will install the command ``fibonacci`` inside your current environment.

Besides console scripts, the header (i.e. until ``_logger``...) of this file can
also be used as template for Python modules.

Note:
    This skeleton file can be safely removed if not needed!

References:
    - https://setuptools.pypa.io/en/latest/userguide/entry_point.html
    - https://pip.pypa.io/en/stable/reference/pip_install
"""

import argparse
import logging
import sys

from palm_tools import __version__
from palm_tools.data_structure.experiment import Experiment
from palm_tools.processing.tif_pipeline import TifPipeline

__author__ = "Hippolyte Verdier"
__copyright__ = "Hippolyte Verdier"
__license__ = "MIT"

_logger = logging.getLogger(__name__)


# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="This command runs localization and tracking on .tif files of PALM acquisition located in --data directory.\n \
            It stores results in the --export directory.\n\
            It can be run with default settings, or custom ones. In the latter case, the user will be prompted to indicates the desired settings.\n\
            If some .tif files were already processed with the given settings, they will not be re-processed"
    )
    parser.add_argument(
        "-n",
        dest="num_processes",
        type=int,
        default=1,
        help="Number of processes to run concurrently",
    )
    run_group = parser.add_mutually_exclusive_group(required=True)
    run_group.add_argument(
        "--default",
        dest="use_default",
        action="store_const",
        const=True,
        default=False,
        help="Use default settings if present",
    )
    run_group.add_argument(
        "--run",
        dest="run_name",
        type=str,
        help="Name of this run, if settings are not default",
    )

    parser.add_argument(
        "--export", dest="export", help="Folder to export processed data", type=str
    )
    parser.add_argument(
        "--data", dest="data", help="Folder where the .tif files are", type=str
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO,
    )
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG,
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`fib` to be called with string arguments in a CLI fashion

    Instead of returning the value from :func:`fib`, it prints the result to the
    ``stdout`` in a nicely formatted message.

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--verbose", "42"]``).
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info("Creating exp...")
    exp = Experiment(data_folder=args.data, export_folder=args.export)
    if args.use_default:
        p_run = exp.default_processing_run
    else:
        p_run = TifPipeline.from_name(name=args.run_name, experiment=exp)
    p_run.process(num_processes=args.num_processes)
    _logger.info("Script ends here")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m palm_tools.skeleton 42
    #
    run()
